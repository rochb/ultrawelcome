package me.sharkz.ultrawelcome.bungee.utils;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Message extends PlaceholdersParsable {

    private final CommandSender sender;

    public Message(CommandSender sender) {
        this.sender = sender;

        if (sender instanceof ProxiedPlayer) setPlayer((ProxiedPlayer) sender);
    }

    @Override
    public void run(String parsedString) {
        sender.sendMessage(parsedString);
    }

}
