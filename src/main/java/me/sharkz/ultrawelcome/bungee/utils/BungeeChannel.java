package me.sharkz.ultrawelcome.bungee.utils;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.bungee.commands.WelcomeBCmd;
import me.sharkz.ultrawelcome.bungee.rewards.actions.Broadcast;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class BungeeChannel implements Listener {

    // Spigot servers that have PlaceholderAPI enabled and that need to have synced data
    private final List<ServerInfo> syncServers = new ArrayList<>();

    public static void parsePlaceholder(ProxiedPlayer player, String s, String s1, UUID id) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("PAPI");
        out.writeUTF(player.getName());
        out.writeUTF(id.toString());
        out.writeUTF(s);
        out.writeUTF(s1);

        if (UWBungee.I.getConfiguration().getPapiType().equalsIgnoreCase("all")) {
            player.getServer().getInfo().sendData(CommonUtils.CHANNEL, out.toByteArray());
        } else {
            ServerInfo server = UWBungee.I.getProxy().getServerInfo(UWBungee.I.getConfiguration().getPapiSingleServer());
            if (server != null && server.getPlayers().size() > 0) {
                server.sendData(CommonUtils.CHANNEL, out.toByteArray());
            } else {
                player.getServer().getInfo().sendData(CommonUtils.CHANNEL, out.toByteArray());
            }
        }
    }

    public static void send(ProxiedPlayer player, String id, String data) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(id);
        out.writeUTF(player.getName());
        out.writeUTF(data);

        player.getServer().getInfo().sendData(CommonUtils.CHANNEL, out.toByteArray());
    }

    public static void send(ProxiedPlayer player, String id, double data) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(id);
        out.writeUTF(player.getName());
        out.writeDouble(data);

        player.getServer().getInfo().sendData(CommonUtils.CHANNEL, out.toByteArray());
    }

    public static void send(ProxiedPlayer player, String id, String s, String s1, int i, int i1, int i2) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(id);
        out.writeUTF(player.getName());
        out.writeUTF(s);
        out.writeUTF(s1);
        out.writeInt(i);
        out.writeInt(i1);
        out.writeInt(i2);

        player.getServer().getInfo().sendData(CommonUtils.CHANNEL, out.toByteArray());
    }

    public static void send(ProxiedPlayer player, String id, String s, int i, int i1) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(id);
        out.writeUTF(player.getName());
        out.writeUTF(s);
        out.writeInt(i);
        out.writeInt(i1);

        player.getServer().getInfo().sendData(CommonUtils.CHANNEL, out.toByteArray());
    }

    public static void send(ProxiedPlayer player, String id, String s, int i) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(id);
        out.writeUTF(player.getName());
        out.writeUTF(s);
        out.writeInt(i);

        player.getServer().getInfo().sendData(CommonUtils.CHANNEL, out.toByteArray());
    }

    @EventHandler
    public void onMessageReceive(PluginMessageEvent e) {
        if (!e.getTag().equals(CommonUtils.CHANNEL)) {
            return;
        }

        ByteArrayDataInput in = ByteStreams.newDataInput(e.getData());

        String channel = in.readUTF();
        ProxiedPlayer player = UWBungee.I.getProxy().getPlayer(in.readUTF());

        switch (channel) {
            case "State": {
                // Add server to spigot servers with UltraWelcome in bungeecord mode
                ServerInfo serverInfo = player.getServer().getInfo();
                if (!UWBungee.I.servers.contains(serverInfo)) UWBungee.I.servers.add(serverInfo);

                // If PAPI is enabled, add server to sync servers and send whole data file for first sync
                boolean papiEnabled = in.readBoolean();
                if (papiEnabled && !syncServers.contains(serverInfo) || serverInfo.getPlayers().size() == 1) {
                    syncServers.add(serverInfo);
                    send(player, "PlayersStorage", UWBungee.I.getPlayers().getStoragetype());
                    Map<String, Integer> points = UWBungee.I.getPlayers().getPoints();
                    Map<String, Integer> numbers = UWBungee.I.getPlayers().getNumbers();
                    UWBungee.I.getPlayers().getPlayers().forEach(s -> send(player, "PlayersData", s, points.get(s), numbers.get(s)));
                }

                // Player spigot server join actions
                playerJoinEvent(player);
                break;
            }
            case "PAPI": {
                UUID id = UUID.fromString(in.readUTF());
                String s = in.readUTF();
                String s1 = in.readUTF();
                UWBungee.I.placeholdersParsables.get(id).updateQueue(s, s1);
                break;
            }
            case "Broadcast": {
                String s = in.readUTF();
                Broadcast broadcast = UWBungee.I.broadcasts.get(player.getUniqueId());
                String message = s.replace("%1$s", player.getDisplayName()).replace("%2$s", broadcast.getParsedString());
                broadcast.getReceivers().forEach(target -> target.sendMessage(message));
                UWBungee.I.broadcasts.remove(player.getUniqueId());
                break;
            }
        }
    }

    @EventHandler
    public void playerPointsUpdate(PlayerPointsUpdateEvent e) {
        syncServers.forEach(serverInfo -> send(new ArrayList<>(serverInfo.getPlayers()).get(0), "PlayersDataUpdate", UWBungee.I.getPlayers().getPlayer(e.getPlayer()), e.getNewPoints()));
    }

    private void playerJoinEvent(ProxiedPlayer player) {
        if (!UWBungee.I.getPlayers().hasPlayedBefore(player)) {
            // Set newest player for welcome command & schedule task to null the welcoming player after a declared time
            WelcomeBCmd.newestPlayer = player.getUniqueId();
            if (WelcomeBCmd.welcoming == null) WelcomeBCmd.welcoming = player.getUniqueId();
            UWBungee.I.servers.stream().filter(serverInfo -> serverInfo.getPlayers().size() > 0).forEach(serverInfo ->
                    send(new ArrayList<>(serverInfo.getPlayers()).get(0), "LatestPlayer", player.getName()));
            UWBungee.I.getProxy().getScheduler().schedule(UWBungee.I, () -> {
                if (WelcomeBCmd.newestPlayer.equals(player.getUniqueId())) WelcomeBCmd.welcoming = null;
            }, WelcomeBCmd.time_limit, TimeUnit.SECONDS);

            // Set player number
            UWBungee.I.getPlayers().addPlayer(player);

            // Send data to sync servers
            send(player, "PlayersData", UWBungee.I.getPlayers().getPlayer(player), 0, UWBungee.I.getPlayers().getPlayerNumber(player));

            // Rewards and actions
            if (UWBungee.I.getConfiguration().isFirstJoinRewards()) {
                UWBungee.I.getConfiguration().getFirstJoinRewards().forEach(reward -> reward.giveReward(player));
            }
            UWBungee.I.getConfiguration().getFirstJoinActions().execute(player, null, Collections.singletonMap("new_player", player));
        } else {
            // Actions
            UWBungee.I.getConfiguration().getJoinActions().execute(player, null, Collections.singletonMap("joined_player", player));
        }
    }
}
