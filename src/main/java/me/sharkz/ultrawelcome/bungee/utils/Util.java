package me.sharkz.ultrawelcome.bungee.utils;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Util {

    /**
     * @param sender
     * @param msg
     */
    public static void sendMessage(CommandSender sender, Lang msg) {
        sendMessage(sender, msg.toString(), new HashMap<>(), new HashMap<>());
    }

    public static void sendMessage(CommandSender sender, String msg) {
        sendMessage(sender, msg, new HashMap<>(), new HashMap<>());
    }

    public static void sendMessage(CommandSender sender, Lang msg, Map<String, String> placeholders) {
        sendMessage(sender, msg.toString(), placeholders, new HashMap<>());
    }

    /**
     * @param sender
     * @param msg
     * @param placeholders
     */
    public static void sendMessage(CommandSender sender, String msg, Map<String, String> placeholders, Map<String, ProxiedPlayer> perPlayerPlaceholders) {
        List<String> tmp = new ArrayList<>();

        tmp.add(msg);

        // CUSTOM PLACEHOLDERS
        if (placeholders == null) placeholders = new HashMap<>();
        placeholders.put("%prefix%", Lang.PREFIX.toString());
        placeholders.put("%player%", sender.getName());
        for (String key : placeholders.keySet()) {
            Map<String, String> finalPlaceholders = placeholders;
            tmp = tmp.stream().map(s -> s.replaceAll(key, finalPlaceholders.get(key))).collect(Collectors.toList());
        }

        // Color
        tmp = CommonUtils.color(tmp);

        // PAPI
        if (UWBungee.I.getConfiguration().isPapiEnabled() && sender instanceof ProxiedPlayer) {
            tmp.forEach(s -> {
                Message message = new Message(sender);
                String result = applyPlaceholders(message, s, new HashMap<>(), (ProxiedPlayer) sender, perPlayerPlaceholders);
                if (result != null) {
                    message.run(result);
                }
            });
        } else {
            tmp.forEach(sender::sendMessage);
        }
    }

    public static void forwardCommand(CommandSender sender, String command, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;

        StringBuilder commandBuilder = new StringBuilder(command);
        for (String arg : args) {
            commandBuilder.append(" ").append(arg);
        }
        command = commandBuilder.toString();

        player.chat(command);
    }

    public static String getSingleOrRandom(Configuration config, String path) {
        String s = "";
        if (config.get(path) instanceof String) {
            s = config.getString(path);
        } else if (config.get(path) instanceof List) {
            List<String> randomS = config.getStringList(path);
            s = randomS.get(new Random().nextInt(randomS.size()));
        }
        return s;
    }

    /**
     * Returns a list of the names of the currently online players on the server.
     *
     * @return List of online players names
     */
    public static List<String> getOnlinePlayerNames() {
        return getPlayerNames(UWBungee.I.getProxy().getPlayers());
    }

    /**
     * Returns a list of the names of the players in the given list.
     *
     * @param players List of players where to get the names
     * @return List of players names
     */
    public static List<String> getPlayerNames(Collection<? extends ProxiedPlayer> players) {
        List<String> l = new ArrayList<>(players.size());
        for (ProxiedPlayer p : players) {
            l.add(p.getName());
        }
        return l;
    }


    /**
     * Applies placeholders to a given string.
     * When you call this method use CompletableFuture#runAsync
     *
     * @param string                String for placeholders ot be applied
     * @param placeholders          Map of strings to be replaced with other strings
     * @param player                Main player for PAPI to retrieve placeholders
     * @param perPlayerPlaceholders Map of placeholders corresponding to players for multiple players placeholders
     * @return String with applied placeholders
     */
    public static String applyPlaceholders(PlaceholdersParsable placeholdersParsable, String string, Map<String, String> placeholders, ProxiedPlayer player, Map<String, ProxiedPlayer> perPlayerPlaceholders) {
        // Defined placeholders
        String result = string;
        for (String k : placeholders.keySet()) {
            result = result.replaceAll(k, placeholders.get(k));
        }

        // PAPI
        if (UWBungee.I.getConfiguration().isPapiEnabled()) {
            placeholdersParsable.setParsedString(result);

            /*
             * Enables the possibility to apply placeholders for multiple players.
             * The regex search for something like this: "%new_player%%luckperms_group%Steve"
             * If it finds "%new_player%" it gets the PAPI placeholder next to it,
             * retrieves the final product and puts it into the string
             *
             */
            for (String k : perPlayerPlaceholders.keySet()) {
                Matcher m = Pattern.compile("(" + k + "(%.+?%))").matcher(result);
                while (m.find()) {
                    placeholdersParsable.addQueueItem(m.group(1));
                    BungeeChannel.parsePlaceholder(perPlayerPlaceholders.get(k), m.group(1), m.group(2), placeholdersParsable.getId());
                }
            }
            if (placeholdersParsable.getParsedString() == null) {
                UWBungee.I.placeholdersParsables.put(placeholdersParsable.getId(), placeholdersParsable);
                BungeeChannel.parsePlaceholder(player, placeholdersParsable.getParsedString(), placeholdersParsable.getParsedString(), placeholdersParsable.getId());
            }

            return null;
        }

        return result;
    }

    public static boolean isSet(Configuration config, String path) {
        return config.get(path, null) != null;
    }

    public static boolean isSection(Configuration config, String path) {
        return config.get(path) instanceof Configuration;
    }

    /**
     * Get permission from config.
     *
     * @param config Configuration file from where to get data
     * @param path   Path of the permission in the config
     * @return Permission if it founds it else null
     */
    public static String getPermission(Configuration config, String path) {
        return isSet(config, path) ?
                config.getString(path).isEmpty() ?
                        null : config.getString(path)
                : null;
    }

    /**
     * Returns a map containing paths and values of a configuration section
     * This is mainly used in this plugin to transfer the items from bungeecord
     * to spigot easily (the map gets serialized and sent as string to spigot
     * that then deserialize it and give the items to the player)
     *
     * @param map  output
     * @param path starting path
     */
    public static void getConfigSectionToMap(Configuration config, HashMap<String, Object> map, String path) {
        for (String option : config.getSection(path).getKeys()) {
            String optionPath = path + "." + option;
            if ((config.get(optionPath)) instanceof Configuration) {
                getConfigSectionToMap(config, map, optionPath);
            } else if ((config.get(optionPath)) instanceof List) {
                map.put(optionPath, config.getList(optionPath));
            } else {
                map.put(optionPath, config.get(optionPath));
            }
        }
    }

    public static void getConfigSectionToMap(Configuration config, HashMap<String, Object> map) {
        for (String option : config.getKeys()) {
            String optionPath = option;
            if ((config.get(optionPath)) instanceof Configuration) {
                getConfigSectionToMap(config, map, optionPath);
            } else if ((config.get(optionPath)) instanceof List) {
                map.put(optionPath, config.getList(optionPath));
            } else {
                map.put(optionPath, config.get(optionPath));
            }
        }
    }
}

