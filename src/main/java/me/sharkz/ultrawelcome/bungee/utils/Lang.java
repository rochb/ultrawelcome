package me.sharkz.ultrawelcome.bungee.utils;

import net.md_5.bungee.config.Configuration;

public enum Lang {

    PREFIX("prefix", "&c&lULTRA&6&lWELCOME &7|"),
    NO_NEW_PLAYER("no_new_player", "%prefix% &eThere is no new player to welcome!"),
    NOT_A_PLAYER("not_a_player", "%prefix% &eOnly players can use this command!"),
    OFFLINE_PLAYER("offline_player", "%prefix% &eThis player is offline!"),
    WELCOME_SELF("welcome_self", "%prefix% &eYou can't welcome yourself!"),
    ALREADY_WELCOME("already_welcome", "%prefix% &eYou have already welcomed this player!"),
    NO_PERMISSION("no_permission", "%prefix% &eYou haven't the permission to do that !"),
    RELOADED("reloaded", "%prefix% &a&lConfiguration has been reloaded!"),
    ANYTHING_TO_DISPLAY("anything_to_display", "&eNo data to display ..."),
    POINTS_COMMAND_HELP("points_command_help", "&bManage player points:" +
            "\n&6Check uw.roch-blondiaux.com to see the command in detail!" +
            "\n&7/uw points add &a<player> <points> &6[rewards]" +
            "\n&7/uw points set &a<player> <points> &6[rewards]" +
            "\n&7/uw points remove &a<player> <points> &6[rewards]" +
            "\n&7/uw points get &a<player>"),
    INVALID_NUMBER("invalid_number", "%prefix% &cPlease enter a valid number!"),
    POINTS_ADDED("points_added", "%prefix% &aPoints added successfully!"),
    POINTS_REMOVED("points_removed", "%prefix% &aPoints removed successfully!"),
    POINTS_SET("points_set", "%prefix% &aPoints set successfully!"),
    POINTS_GET("points_get", "%prefix% &6%player% &ahas &6%points% &apoints!"),
    INVALID_SUBCOMMAND("invalid_subcommand", "%prefix% &cThat subcommand doesn't exist!"),
    WELCOME_COMMAND_HELP("welcome_command_help", "&bWelcome command help:" +
            "\n&6/welcome &8- &7Welcome the newest player" +
            "\n&6/welcome top &8- &7Display the points leaderboard" +
            "\n&6/welcome help &8- &7Display this help message"),
    ;


    private String path;
    private String def;
    private static Configuration LANG;

    /**
     * Lang enum constructor.
     * @param path The string path.
     * @param start The default string.
     */
    Lang(String path, String start) {
        this.path = path;
        this.def = start;
    }

    /**
     * Set the {@code YamlConfiguration} to use.
     * @param config The config to set.
     */
    public static void setFile(Configuration config) {
        LANG = config;
    }

    @Override
    public String toString() {
        return LANG.getString(this.path, def).replace("%prefix%", LANG.getString(PREFIX.path, PREFIX.def));
    }

    /**
     * Get the default value of the path.
     * @return The default value of the path.
     */
    public String getDefault() {
        return this.def;
    }

    /**
     * Get the path to the string.
     * @return The path to the string.
     */
    public String getPath() {
        return this.path;
    }
}
