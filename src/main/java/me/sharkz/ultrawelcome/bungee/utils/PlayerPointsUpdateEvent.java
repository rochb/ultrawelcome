package me.sharkz.ultrawelcome.bungee.utils;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Event;

public class PlayerPointsUpdateEvent extends Event {

    private final ProxiedPlayer player;
    private final int oldPoints;
    private final int newPoints;

    public PlayerPointsUpdateEvent(ProxiedPlayer player, int oldPoints, int newPoints) {
        this.player = player;
        this.oldPoints = oldPoints;
        this.newPoints = newPoints;
    }

    public ProxiedPlayer getPlayer() {
        return player;
    }

    public int getOldPoints() {
        return oldPoints;
    }

    public int getNewPoints() {
        return newPoints;
    }
}
