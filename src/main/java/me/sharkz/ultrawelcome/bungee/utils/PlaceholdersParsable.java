package me.sharkz.ultrawelcome.bungee.utils;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class PlaceholdersParsable {

    private final UUID id;
    private final List<String> queue = new ArrayList<>();
    private String parsedString;
    private ProxiedPlayer player;

    public PlaceholdersParsable() {
        this.id = UUID.randomUUID();
    }

    public UUID getId() {
        return this.id;
    }

    public void setPlayer(ProxiedPlayer player) {
        this.player = player;
    }

    public void run(String parsedString) {

    }

    public int getQueueCount() {
        return this.queue.size();
    }

    public void addQueueItem(String item) {
        UWBungee.I.placeholdersParsables.put(this.id, this);
        this.queue.add(item);
    }

    public void removeQueueItem(String item) {
        this.queue.remove(item);
    }

    public void updateQueue(String item, String parsedItem) {
        if (getQueueCount() == 0) {
            this.parsedString = parsedItem;
            UWBungee.I.placeholdersParsables.remove(this.id);
            run(this.parsedString);
        } else {
            removeQueueItem(item);
            this.parsedString = this.parsedString.replace(item, parsedItem);
            if (getQueueCount() == 0) {
                BungeeChannel.parsePlaceholder(player, this.parsedString, this.parsedString, getId());
            }
        }
    }

    public String getParsedString() {
        return parsedString;
    }

    public void setParsedString(String parsedString) {
        this.parsedString = parsedString;
    }
}
