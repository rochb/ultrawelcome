package me.sharkz.ultrawelcome.bungee.rewards;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.bungee.utils.BungeeChannel;
import me.sharkz.ultrawelcome.bungee.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.IOException;
import java.util.HashMap;

public class Reward {

    private final String message;
    private final int points;
    private final double money;
    private final String command;
    private final String commandType;
    private final HashMap<String, Object> items;

    public Reward(String message, int points, double money, String command, String commandType) {
        this.message = message;
        this.points = points;
        this.money = money;
        this.command = command;
        this.commandType = commandType;
        this.items = new HashMap<>();
    }

    public Reward(String message, int points, double money, String command, String commandType, HashMap<String, Object> items) {
        this.message = message;
        this.points = points;
        this.money = money;
        this.command = command;
        this.commandType = commandType;
        this.items = items;
    }

    public boolean hasMessage() {
        return message != null && !message.isEmpty();
    }

    public boolean hasPoints() {
        return points != 0;
    }

    public boolean hasMoney() {
        return money != 0;
    }

    public boolean hasCommand() {
        return command != null && !message.isEmpty();
    }

    public boolean hasCommandType() {
        return commandType != null;
    }

    public boolean hasItems() {
        return items != null && !items.isEmpty();
    }

    public String getMessage() {
        return message;
    }

    public int getPoints() {
        return points;
    }

    public double getMoney() {
        return money;
    }

    public String getCommand() {
        return command;
    }

    public String getCommandType() {
        return hasCommandType() ? commandType : "bungee";
    }

    public HashMap<String, Object> getItems() {
        return items;
    }

    public void giveReward(ProxiedPlayer player) {
        if (hasMessage()) {
            Util.sendMessage(player, getMessage());
        }
        if (hasMoney()) {
            BungeeChannel.send(player, "money", getMoney());
        }
        if (hasCommand()) {
            String cmd = getCommand();
            if (cmd.startsWith("/")) cmd = cmd.replaceFirst("/", "");
            if (cmd.contains("%player%")) cmd = cmd.replace("%player%", player.getName());
            if (getCommandType().equalsIgnoreCase("bungee")) {
                UWBungee.I.getProxy().getPluginManager().dispatchCommand(UWBungee.I.getProxy().getConsole(), cmd);
            } else if (getCommandType().equalsIgnoreCase("spigot")) {
                BungeeChannel.send(player, "command", cmd);
            }
        }
        if (hasItems()) {
            try {
                BungeeChannel.send(player, "items", CommonUtils.serialize(getItems()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
