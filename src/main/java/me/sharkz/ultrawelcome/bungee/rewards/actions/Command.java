package me.sharkz.ultrawelcome.bungee.rewards.actions;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.bungee.utils.BungeeChannel;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Map;

public class Command {

    private final ActionType actionType;
    private final String command;
    private final String commandType;
    private final String permission;

    public Command(ActionType actionType, String command, String commandType, String permission) {
        this.actionType = actionType;
        this.command = command;
        this.commandType = commandType;
        this.permission = permission;
    }

    public void run(ProxiedPlayer sender, ProxiedPlayer newPlayer, Map<String ,String> placeholders) {
        String tempCmd = command.trim();
        if (tempCmd.startsWith("/")) tempCmd = tempCmd.substring(1);
        for (String s : placeholders.keySet()) {
            if (!s.equals("%player%")) tempCmd = tempCmd.replace(s, placeholders.get(s));
        }
        String cmd = tempCmd;
        switch (actionType) {
            case GLOBAL: {
                UWBungee.I.getProxy().getPlayers().forEach(player -> {
                    if (permission == null || player.hasPermission(permission))
                        if (commandType.equalsIgnoreCase("bungee")) {
                            UWBungee.I.getProxy().getPluginManager().dispatchCommand(UWBungee.I.getProxy().getConsole(), cmd.replace("%player%", player.getName()));
                        } else if (commandType.equalsIgnoreCase("spigot")) {
                            BungeeChannel.send(player, "command", cmd);
                        }
                });
                break;
            }
            case SINGLE: {
                if (permission == null || sender.hasPermission(permission)) {
                    UWBungee.I.getProxy().getPluginManager().dispatchCommand(UWBungee.I.getProxy().getConsole(), cmd.replace("%player%", placeholders.get("%player%")));
                }
                break;
            }
        }
    }
}
