package me.sharkz.ultrawelcome.bungee.rewards.actions;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.bungee.utils.BungeeChannel;
import me.sharkz.ultrawelcome.bungee.utils.PlaceholdersParsable;
import me.sharkz.ultrawelcome.bungee.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Map;

public class Title extends PlaceholdersParsable {

    private final ActionType actionType;
    private final String title;
    private final String subtitle;
    private final int fadeIn;
    private final int stay;
    private final int fadeOut;
    private final String permission;

    private ProxiedPlayer sender;
    private ProxiedPlayer newPlayer;
    private String parsedTitle;
    private String parsedSubtitle;
    private Map<String, String> placeholders;
    private Map<String, ProxiedPlayer> perPlayerPlaceholders;

    public Title(ActionType actionType, String title, String subtitle, int fadeIn, int stay, int fadeOut, String permission) {
        this.actionType = actionType;
        this.title = title;
        this.subtitle = subtitle;
        this.fadeIn = fadeIn;
        this.stay = stay;
        this.fadeOut = fadeOut;
        this.permission = permission;
    }

    public void run(ProxiedPlayer sender, ProxiedPlayer newPlayer, Map<String, String> placeholders, Map<String, ProxiedPlayer> perPlayerPlaceholders) {
        this.sender = sender;
        this.newPlayer = newPlayer;
        this.placeholders = placeholders;
        this.perPlayerPlaceholders = perPlayerPlaceholders;

        setPlayer(newPlayer);

        String result = Util.applyPlaceholders(this, title, placeholders, sender, perPlayerPlaceholders);
        if (result != null) {
            run(result);
        }
    }

    @Override
    public void run(String parsedString) {
        parsedString = CommonUtils.color(parsedString);
        if (this.parsedTitle == null) {
            this.parsedTitle = parsedString;
            String result = Util.applyPlaceholders(this, subtitle, placeholders, sender, perPlayerPlaceholders);
            if (result != null) {
                run(result);
            }
            return;
        } else if (this.parsedSubtitle == null) {
            this.parsedSubtitle = parsedString;
        }

        switch (actionType) {
            case GLOBAL: {
                UWBungee.I.getProxy().getPlayers().forEach(player -> BungeeChannel.send(player, "title", parsedTitle, parsedSubtitle, fadeIn, stay, fadeOut));
                break;
            }
            case SINGLE: {
                if (sender != null && (permission == null || sender.hasPermission(permission))) BungeeChannel.send(sender, "title", parsedTitle, parsedSubtitle, fadeIn, stay, fadeOut);
                if (newPlayer != null && (permission == null || newPlayer.hasPermission(permission))) BungeeChannel.send(newPlayer, "title", parsedTitle, parsedSubtitle, fadeIn, stay, fadeOut);
                break;
            }
        }
    }
}
