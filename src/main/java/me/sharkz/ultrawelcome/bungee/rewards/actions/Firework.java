package me.sharkz.ultrawelcome.bungee.rewards.actions;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.bungee.utils.BungeeChannel;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.IOException;
import java.util.HashMap;

public class Firework {

    private final ActionType actionType;
    private final HashMap<String, Object> fireworkMeta;
    private final String permission;

    public Firework(ActionType actionType, HashMap<String, Object> fireworkMeta, String permission) {
        this.actionType = actionType;
        this.fireworkMeta = fireworkMeta;
        this.permission = permission;
    }

    public void run(ProxiedPlayer sender, ProxiedPlayer newPlayer) {
        switch (actionType) {
            case GLOBAL: {
                UWBungee.I.getProxy().getPlayers().forEach(player -> {
                    if (permission == null || player.hasPermission(permission)) {
                        try {
                            BungeeChannel.send(player, "firework", CommonUtils.serialize(fireworkMeta));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                break;
            }
            case SINGLE: {
                try {
                    if (permission == null || sender.hasPermission(permission)) BungeeChannel.send(sender, "firework", CommonUtils.serialize(fireworkMeta));
                    if (newPlayer != null && permission == null || newPlayer.hasPermission(permission)) BungeeChannel.send(newPlayer, "firework", CommonUtils.serialize(fireworkMeta));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

}
