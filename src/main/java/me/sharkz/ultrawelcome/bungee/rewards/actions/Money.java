package me.sharkz.ultrawelcome.bungee.rewards.actions;

import me.sharkz.ultrawelcome.bungee.utils.BungeeChannel;
import me.sharkz.ultrawelcome.bungee.utils.PlaceholdersParsable;
import me.sharkz.ultrawelcome.bungee.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Map;

public class Money extends PlaceholdersParsable {

    private final int money;
    private final String message;
    private final String permission;

    private ProxiedPlayer sender;

    public Money(int money, String message, String permission) {
        this.money = money;
        this.message = message;
        this.permission = permission;
    }

    public void run(ProxiedPlayer sender, Map<String, String> placeholders, Map<String, ProxiedPlayer> perPlayerPlaceholders) {
        this.sender = sender;

        setPlayer(sender);

        if (sender != null && money > 0 && (permission == null || sender.hasPermission(permission))) {
            if (message != null && !message.isEmpty()) {
                String result = Util.applyPlaceholders(this, message, placeholders, sender, perPlayerPlaceholders);
                if (result != null) {
                    run(CommonUtils.color(result));
                }
            }
        }
    }

    @Override
    public void run(String parsedString) {
        BungeeChannel.send(sender, "money", money);
        sender.sendMessage(parsedString);
    }

}
