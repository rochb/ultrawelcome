package me.sharkz.ultrawelcome.bungee.rewards.actions;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.bungee.utils.BungeeChannel;
import me.sharkz.ultrawelcome.bungee.utils.PlaceholdersParsable;
import me.sharkz.ultrawelcome.bungee.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Broadcast extends PlaceholdersParsable {

    private final ActionType actionType;
    private final String content;
    private List<ProxiedPlayer> receivers;
    private final String permission;

    private ProxiedPlayer sender;
    private ProxiedPlayer newPlayer;


    public Broadcast(ActionType actionType, String content, String permission) {
        this.actionType = actionType;
        this.content = content;
        this.permission = permission;
    }

    public String getContent() {
        return this.content;
    }

    public List<ProxiedPlayer> getReceivers() {
        return this.receivers;
    }

    public String getPermission() {
        return this.permission;
    }

    public void setReceivers(List<ProxiedPlayer> receivers) {
        this.receivers = receivers;
    }

    public void run(ProxiedPlayer sender, ProxiedPlayer newPlayer, Map<String, String> placeholders, Map<String, ProxiedPlayer> perPlayerPlaceholders) {
        this.sender = sender;
        this.newPlayer = newPlayer;

        setPlayer(newPlayer);

        String result = Util.applyPlaceholders(this, content, placeholders, sender, perPlayerPlaceholders);
        if (result != null) {
            run(CommonUtils.color(result));
        }
    }

    @Override
    public void run(String parsedString) {
        switch (actionType) {
            case GLOBAL:
            case GLOBAL_BC: {
                UWBungee.I.getProxy().getPlayers().forEach(player -> {
                    if (permission == null || player.hasPermission(permission)) player.sendMessage(parsedString);
                });
                break;
            }
            case SINGLE:
            case SINGLE_BC: {
                if (sender != null && (permission == null || sender.hasPermission(permission)))
                    sender.sendMessage(parsedString);
                if (newPlayer != null && (permission == null || newPlayer.hasPermission(permission)))
                    newPlayer.sendMessage(parsedString);
                break;
            }
            case GLOBAL_CHAT: {
                if (sender != null) {
                    setReceivers(permission == null ? new ArrayList<>(UWBungee.I.getProxy().getPlayers()) : UWBungee.I.getProxy().getPlayers().stream().filter(player -> player.hasPermission(permission)).collect(Collectors.toList()));
                    UWBungee.I.broadcasts.put(sender.getUniqueId(), this);
                    BungeeChannel.send(sender, "Broadcast", parsedString);
                }
                break;
            }
            case SINGLE_CHAT: {
                List<ProxiedPlayer> rc = new ArrayList<>();
                if (sender != null) {
                    if (permission == null || sender.hasPermission(permission)) rc.add(sender);
                    if (newPlayer != null && (permission == null || newPlayer.hasPermission(permission)))
                        rc.add(newPlayer);
                    setReceivers(rc);
                    UWBungee.I.broadcasts.put(sender.getUniqueId(), this);
                    BungeeChannel.send(sender, "Broadcast", parsedString);
                }
                break;
            }
        }
    }
}
