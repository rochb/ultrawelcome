package me.sharkz.ultrawelcome.bungee;

import com.google.common.io.Files;
import me.sharkz.ultrawelcome.bungee.commands.UWBCmd;
import me.sharkz.ultrawelcome.bungee.commands.WelcomeBCmd;
import me.sharkz.ultrawelcome.bungee.files.Config;
import me.sharkz.ultrawelcome.bungee.files.PlayersData;
import me.sharkz.ultrawelcome.bungee.listeners.UBListener;
import me.sharkz.ultrawelcome.bungee.rewards.actions.Broadcast;
import me.sharkz.ultrawelcome.bungee.utils.*;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import org.bstats.bungeecord.Metrics;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

public class UWBungee extends Plugin {

    public static UWBungee I;
    public static Logger L;
    public boolean newVersion;
    public final int configVersion = 8;

    private Config config;
    private PlayersData playersData;

    public Map<UUID, Broadcast> broadcasts = new HashMap<>();
    public List<ServerInfo> servers = new ArrayList<>();
    public Map<UUID, PlaceholdersParsable> placeholdersParsables = new HashMap<>();

    public UWBungee() {
        I = this;
    }

    @Override
    public void onEnable() {
        L = this.getLogger();

        // Header start
        L.info("========== ULTRA WELCOME ==========");
        L.info("Developed by : Sharkz, Quantum");
        L.info("Version : " + getDescription().getVersion());
        L.info("Do not hesitate to positively rate the plugin on spigotmc.org");
        L.info("===================================");

        // Updates
        newVersion = false;
        checkVersion();

        // Metrics
        new Metrics(this, 8665);

        // Configuration
        if (!getDataFolder().exists()) getDataFolder().mkdir();
        config = new Config();
        if (getConfiguration().getConfig().getInt("config_version") != configVersion) {
            L.warning("The configuration file is outdated !");
            try {
                Files.copy(new File(getDataFolder(), "config.yml"), new File(getDataFolder(), "config.old.yml"));
                new File(getDataFolder(), "config.yml").delete();
                config = new Config();
                L.info("Your configuration has been saved & replaced !");
            } catch (IOException e) {
                L.warning("Cannot backup your configuration ...");
                e.printStackTrace();
            }
        }
        playersData = new PlayersData();
        playersData.updateForOlderVersions();

        // Lang file
        loadLang();

        // Messaging Channel
        getProxy().getPluginManager().registerListener(this, new BungeeChannel());
        getProxy().registerChannel(CommonUtils.CHANNEL);

        // Listeners
        getProxy().getPluginManager().registerListener(this, new UBListener());

        // Commands
        new UWBCmd();
        if (!getConfiguration().getConfig().getString("welcomeCommand.command", "welcome").isEmpty()) {
            new WelcomeBCmd(getConfiguration().getConfig().getString("welcomeCommand.command", "welcome"),
                    Util.getPermission(getConfiguration().getConfig(), "welcomeCommand.permission"),
                    getConfiguration().getConfig().getStringList("welcomeCommand.aliases"),
                    getConfiguration().getConfig().getInt("welcomeCommand.time-limit", 12));
        }
    }

    /**
     * Check if a new version of the plugin exist
     */
    private void checkVersion() {
        new UpdateChecker(this, 82991)
                .getVersion(s -> {
                    if (getDescription().getVersion().equalsIgnoreCase(s))
                        L.info("You're running the last version of the plugin !");
                    else {
                        L.warning("A new version of UltraWelcome is available on spigotmc.org !");
                        newVersion = true;
                    }
                });
    }

    @Override
    public void onDisable() {
        L.info("Thanks for using UltraWelcome !");
        playersData.save();
    }

    public Config getConfiguration() {
        return config;
    }

    public PlayersData getPlayers() {
        return playersData;
    }

    public void loadLang() {
        String lang = getConfiguration().getLang();
        File langFile = new File(getDataFolder() + "/lang", lang + ".lang");

        if (!langFile.exists()) {
            langFile.getParentFile().mkdirs();
            try {
                langFile.createNewFile();
                FileWriter writer = new FileWriter(langFile, true);
                writer.write("# All messages support %prefix% placeholder\n" +
                        "# Messages starting with \"points\" (except \"points_command_help\") support %player% and %points%\n" +
                        "# For yaml multi line messages see uw.roch-blondiaux.com/YamlFormatting");
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Configuration langConfig = null;
        try {
            langConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(langFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create lang file with the name specified by the user if it doesn't exist
        if (!langFile.exists()) {
            try {
                langFile.getParentFile().mkdirs();
                langFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // For avoiding null values we put default ones in the lang file if they doesn't exist
        for (Lang item : Lang.values()) {
            if (langConfig.getString(item.getPath(), null) == null) {
                langConfig.set(item.getPath(), item.getDefault());
            }
        }

        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(langConfig, langFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // We set the configuration variable in the Lang enum so it can use when we request a message
        Lang.setFile(langConfig);
    }
}
