package me.sharkz.ultrawelcome.bungee.files;

import com.google.common.io.ByteStreams;
import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.bungee.loader.ActionsLoader;
import me.sharkz.ultrawelcome.bungee.loader.FirstJoinRewardLoader;
import me.sharkz.ultrawelcome.bungee.loader.RewardLoader;
import me.sharkz.ultrawelcome.bungee.rewards.Actions;
import me.sharkz.ultrawelcome.bungee.rewards.Reward;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;
import java.util.List;

public class Config {

    private Configuration config;
    private final File file = new File(UWBungee.I.getDataFolder(), "config.yml");

    private String lang;

    private Actions welcomeActions;
    private Actions firstJoinActions;
    private Actions joinActions;

    private List<Reward> milestones;
    private List<Reward> firstJoinRewards;

    private boolean firstJoinRewardsEnabled;

    private boolean notifyUpdateOnJoin;
    private boolean papiEnabled;
    private String papiType;
    private String papiSingleServer;

    public Config() {
        if (!file.exists()) {
            try (InputStream in = UWBungee.I.getResourceAsStream("config_bungee.yml"); OutputStream os = new FileOutputStream(file)) {
                ByteStreams.copy(in, os);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        reload();
    }

    public Configuration getConfig() {
        return config;
    }

    public void reload() {
        try {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
        } catch (IOException e) {
            UWBungee.L.warning("Cannot load config.yml !");
            e.printStackTrace();
        }

        this.lang = config.getString("lang", "en_US");

        this.welcomeActions = new ActionsLoader().load(config, "onWelcome", true);
        this.firstJoinActions = new ActionsLoader().load(config, "first-join", false);
        this.joinActions = new ActionsLoader().load(config, "join", false);

        this.milestones = new RewardLoader().getRewards(config);
        this.firstJoinRewards = new FirstJoinRewardLoader().getRewards(config);

        this.firstJoinRewardsEnabled = config.getBoolean("first-join-rewards.enabled", false);

        this.notifyUpdateOnJoin = config.getBoolean("notify-update-on-join", true);
        this.papiEnabled = config.getBoolean("placeholderapi.enabled", false);
        this.papiType = config.getString("placeholderapi.type", "all");
        this.papiSingleServer = config.getString("placeholderapi.single-server", null);
    }

    public String getLang() {
        return lang;
    }

    public Actions getWelcomeActions() {
        return welcomeActions;
    }

    public Actions getFirstJoinActions() {
        return firstJoinActions;
    }

    public Actions getJoinActions() {
        return joinActions;
    }

    public List<Reward> getMilestones() {
        return milestones;
    }

    public List<Reward> getFirstJoinRewards() {
        return firstJoinRewards;
    }

    public boolean isFirstJoinRewards() {
        return firstJoinRewardsEnabled;
    }

    public boolean isNotifyUpdateOnJoin() {
        return notifyUpdateOnJoin;
    }

    public boolean isPapiEnabled() {
        return papiEnabled;
    }

    public String getPapiType() {
        return papiType;
    }

    public String getPapiSingleServer() {
        return papiSingleServer;
    }
}
