package me.sharkz.ultrawelcome.bungee.files;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.bungee.utils.PlayerPointsUpdateEvent;
import me.sharkz.ultrawelcome.bungee.utils.Util;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class PlayersData {

    private Configuration config;
    private final File file = new File(UWBungee.I.getDataFolder(), "players.yml");

    private final String storagetype = UWBungee.I.getConfiguration().getConfig().getString("player-storage-type", "uuid");

    public PlayersData() {
        if (!file.exists()) {
            try {
                file.createNewFile();
                setHeader("# ====================================== #\n" +
                        "# UltraWelcome                           #\n" +
                        "# Version " + UWBungee.I.getDescription().getVersion() + "             #\n" +
                        "# Developed by Sharkz, Quantum           #\n" +
                        "#                                        #\n" +
                        "# Don't edit this file if you don't      #\n" +
                        "# know what you are doing                #\n" +
                        "# ====================================== #\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        reload();
    }

    private void setHeader(String header) throws IOException {
        FileWriter writer = new FileWriter(file, true);
        writer.write(header);
        writer.close();
    }

    public void reload() {
        try {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
        } catch (IOException e) {
            UWBungee.L.warning("Cannot load players.yml !");
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, file);
        } catch (IOException e) {
            UWBungee.L.warning("Cannot save players.yml !");
            e.printStackTrace();
        }
    }

    public String getStoragetype() {
        return storagetype;
    }

    /**
     * Get the player UUID or NAME based on the storage type set.
     *
     * @param player Player from where to get the info
     * @return Path to player data in config
     */
    public String getPlayer(ProxiedPlayer player) {
        if (storagetype.equals("uuid")) {
            return player.getUniqueId().toString();
        } else if (storagetype.equals("name")) {
            return player.getName();
        }
        return null;
    }

    /**
     * Get the player points from the config.
     *
     * @param player Player to get points
     * @return Player points
     */
    public int getPoints(ProxiedPlayer player) {
        return config.getInt(getPlayer(player) + ".points", 0);
    }

    /**
     * Add player points to the previously saved in the config.
     *
     * @param player Player to add points
     * @param points Points to be added
     */
    public void addPoints(ProxiedPlayer player, int points) {
        setPoints(player, getPoints(player) + points);
    }

    /**
     * Set player points overriding the previously saved in the config.
     *
     * @param player Player to set points
     * @param points Points to set
     */
    public void setPoints(ProxiedPlayer player, int points) {
        UWBungee.I.getProxy().getPluginManager().callEvent(new PlayerPointsUpdateEvent(player, getPoints(player), points));
        config.set(getPlayer(player) + ".points", points);
        save();
    }

    /**
     * Removes player points to the previously saved in the config.
     *
     * @param player Player to remove points
     * @param points Points to remove
     */
    public void removePoints(ProxiedPlayer player, int points) {
        setPoints(player, getPoints(player) - points);
    }

    /**
     * Get all saved players with their points from the config
     *
     * @return Map containing UUID or NAME of the player and its points
     */
    public Map<String, Integer> getPoints() {
        Map<String, Integer> tmp = new HashMap<>();
        config.getKeys().forEach(s -> tmp.put(s, config.getInt(s + ".points", 0)));
        return sortByValue(tmp);
    }

    public Map<String, Integer> getNumbers() {
        Map<String, Integer> tmp = new HashMap<>();
        config.getKeys().forEach(s -> tmp.put(s, config.getInt(s + ".number")));
        return sortByValue(tmp);
    }

    /**
     *
     * @param map
     * @param <K>
     * @param <V>
     * @return
     */
    private <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list)
            result.put(entry.getKey(), entry.getValue());

        return result;
    }

    /**
     * Gets the player number from the config
     *
     * @param player Player to get its number
     * @return Player number
     */
    public int getPlayerNumber(ProxiedPlayer player) {
        return config.getInt(getPlayer(player) + ".number");
    }

    /**
     * Set player number in config
     *
     * @param player Player to set in config
     */
    public void addPlayer(ProxiedPlayer player) {
        config.set(getPlayer(player) + ".number", config.getKeys().stream().filter(s -> Util.isSet(config, s + ".number")).count());
    }

    /**
     * Return if a player has joined the server before
     *
     * @param player Player to check
     * @return If the player has joined before
     */
    public boolean hasPlayedBefore(ProxiedPlayer player) {
        return Util.isSet(config, getPlayer(player));
    }

    /**
     * Get all players set in the config
     *
     * @return List of players in the config
     */
    public List<String> getPlayers() {
        return new ArrayList<>(config.getKeys());
    }

    /**
     * Older versions of UltraWelcome doesn't support "playerNumber" so we
     * prevent throwing some errors in newer versions by adding the "playerNumber".
     */
    public void updateForOlderVersions() {
        AtomicInteger playerNumber = new AtomicInteger((int) config.getKeys().stream().filter(s -> Util.isSet(config, s + ".number")).count());
        if (playerNumber.get() == config.getKeys().size())
            config.getKeys().forEach(s -> {
                if (!Util.isSection(config, s) && !Util.isSet(config, s + ".number")) {
                    int points = config.getInt(s);
                    config.set(s + ".points", points);
                    config.set(s + ".number", playerNumber.get());
                    playerNumber.addAndGet(1);
                }
            });
    }
}
