package me.sharkz.ultrawelcome.bungee.commands;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.bungee.commands.sub.ISubBCmd;
import me.sharkz.ultrawelcome.bungee.commands.sub.SPointsBCmd;
import me.sharkz.ultrawelcome.bungee.commands.sub.SReloadBCmd;
import me.sharkz.ultrawelcome.bungee.utils.Lang;
import me.sharkz.ultrawelcome.bungee.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.*;

public class UWBCmd extends UBCmd {

    private final HashMap<String, ISubBCmd> subCommands = new HashMap<>();

    public UWBCmd() {
        super("ultrawelcome", "", Collections.singletonList("uw"));

        subCommands.put("reload", new SReloadBCmd());
        subCommands.put("points", new SPointsBCmd());
    }

    public Plugin getPlugin() {
        return UWBungee.I;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer && !UWBungee.I.servers.contains(((ProxiedPlayer) sender).getServer().getInfo())) {
            Util.forwardCommand(sender, "/ultrawelcome", args);
            return;
        }

        if (args.length == 0) {
            Util.sendMessage(sender, "%prefix% &7Version : &a&l" + UWBungee.I.getDescription().getVersion());
            return;
        }

        ISubBCmd subCmd = subCommands.get(args[0]);
        if (subCmd == null) {
            Util.sendMessage(sender, Lang.INVALID_SUBCOMMAND);
            return;
        }
        subCmd.execute(sender, Arrays.copyOfRange(args, 1, args.length));
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length == 1) {
            return CommonUtils.filterTabCompleteOptions(subCommands.keySet(), args);
        }

        ISubBCmd subCmd = subCommands.get(args[0]);
        if (subCmd == null) return new ArrayList<>();
        return subCmd.onTabComplete(sender, Arrays.copyOfRange(args, 1, args.length));
    }
}
