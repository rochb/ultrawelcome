package me.sharkz.ultrawelcome.bungee.commands;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.List;

public abstract class UBCmd extends Command implements TabExecutor {

    private boolean register = false;

    public UBCmd(String name, String permission, List<String> aliases) {
        super(name, permission, aliases.toArray(new String[0]));
        registerCommand();
    }

    /**
     * Register the command
     *
     */
    public void registerCommand() {
        if (!register)
            register = true;
            UWBungee.I.getProxy().getPluginManager().registerCommand(UWBungee.I, this);
    }
}
