package me.sharkz.ultrawelcome.bungee.commands.sub;

import net.md_5.bungee.api.CommandSender;

public interface ISubBCmd {

    void execute(CommandSender sender, String[] args);

    Iterable<String> onTabComplete(CommandSender sender, String[] args);

}
