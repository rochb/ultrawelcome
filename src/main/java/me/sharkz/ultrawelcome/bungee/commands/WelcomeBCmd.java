package me.sharkz.ultrawelcome.bungee.commands;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.bungee.files.PlayersData;
import me.sharkz.ultrawelcome.bungee.utils.Lang;
import me.sharkz.ultrawelcome.bungee.utils.Util;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class WelcomeBCmd extends UBCmd {

    public static UUID newestPlayer;
    public static UUID welcoming;
    private final List<UUID> welcomers = new ArrayList<>();
    public static int time_limit = 12;

    public WelcomeBCmd(String name, String permission, List<String> aliases, int time) {
        super(name, permission, aliases);
        time_limit = time;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        List<String> tmp = new ArrayList<>();
        if (args.length == 1) tmp.add("top");
        return tmp;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        Configuration config = UWBungee.I.getConfiguration().getConfig();
        PlayersData playersData = UWBungee.I.getPlayers();
        if (sender instanceof ProxiedPlayer && !UWBungee.I.servers.contains(((ProxiedPlayer) sender).getServer().getInfo())) {
            Util.forwardCommand(sender, "/" + this.getName(), args);
            return;
        }
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("top")) {
                List<String> header = new ArrayList<>();
                if (config.get("top.header") instanceof List) header = config.getStringList("top.header");
                else header.add(config.getString("top.header", "&7Top welcomers : "));
                header.forEach(s -> Util.sendMessage(sender, s));
                Map<String, Integer> tmp = playersData.getPoints();
                if (tmp.size() == 0)
                    Util.sendMessage(sender, Lang.ANYTHING_TO_DISPLAY);
                else {
                    AtomicInteger rank = new AtomicInteger(1);
                    tmp.forEach((s, integer) -> {
                        if (rank.get() <= config.getInt("top.length", 10)) {
                            Util.sendMessage(sender,
                                    config.getString("top.format", "&7N°&a%rank% - &a%rankedPlayer%&7 &b%points%&7 points"),
                                    topSubCommandPlaceholders(rank, s, integer),
                                    sender instanceof ProxiedPlayer ? Collections.singletonMap("ph_rankedPlayer", (ProxiedPlayer) sender) : new HashMap<>()
                            );
                        }
                    });
                }
            } else if (args[0].equalsIgnoreCase("help")) {
                Util.sendMessage(sender, Lang.WELCOME_COMMAND_HELP);
            }
        } else {
            // Console check
            if (!(sender instanceof ProxiedPlayer)) {
                Util.sendMessage(sender, Lang.NOT_A_PLAYER);
                return;
            }
            ProxiedPlayer player = (ProxiedPlayer) sender;
            // Permission check
            if (getPermission() != null && !player.hasPermission(getPermission())) {
                Util.sendMessage(sender, Lang.NO_PERMISSION);
                return;
            }
            // New player check
            if (welcoming == null) {
                Util.sendMessage(player, Lang.NO_NEW_PLAYER);
                return;
            }
            // Target check
            ProxiedPlayer target = UWBungee.I.getProxy().getPlayer(newestPlayer);
            if (target == null || !target.isConnected()) {
                Util.sendMessage(player, Lang.OFFLINE_PLAYER);
                return;
            }
            // Self check
            if (newestPlayer.equals(player.getUniqueId())) {
                Util.sendMessage(player, Lang.WELCOME_SELF);
                return;
            }
            // Variable reset
            if (!newestPlayer.equals(welcoming)) {
                welcoming = newestPlayer;
                welcomers.clear();
            }
            // Already welcome check
            if (welcomers.contains(player.getUniqueId())) {
                Util.sendMessage(player, Lang.ALREADY_WELCOME);
                return;
            }

            Map<String, ProxiedPlayer> placeholders = new HashMap<>();
            placeholders.put("player", player);
            placeholders.put("new_player", target);
            UWBungee.I.getConfiguration().getWelcomeActions().execute(player, target, placeholders);

            // Add points to player
            playersData.addPoints(player, 1);

            // Rewards
            UWBungee.I.getConfiguration().getMilestones().stream().filter(reward -> reward.getPoints() == playersData.getPoints(player)).forEach(reward -> reward.giveReward(player));

            welcomers.add(player.getUniqueId());
        }
    }

    private Map<String, String> topSubCommandPlaceholders(AtomicInteger rank, String s, int integer) {
        Map<String, String> map = new HashMap<>();
        map.put("%rank%", String.valueOf(rank.getAndIncrement()));
        map.put("%points%", String.valueOf(integer));
        map.put("%rankedPlayer%", s);

        return map;
    }
}
