package me.sharkz.ultrawelcome.bungee.commands.sub;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.bungee.utils.Util;
import me.sharkz.ultrawelcome.bungee.utils.Lang;
import net.md_5.bungee.api.CommandSender;

import java.util.ArrayList;

public class SReloadBCmd implements ISubBCmd {

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!sender.hasPermission("ultrawelcome.reload")) {
            Util.sendMessage(sender, Lang.NO_PERMISSION);
            return;
        }

        UWBungee.I.getConfiguration().reload();
        UWBungee.I.loadLang();
        Util.sendMessage(sender, Lang.RELOADED);
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        return new ArrayList<>();
    }
}
