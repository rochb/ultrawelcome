package me.sharkz.ultrawelcome.bungee.loader;

import net.md_5.bungee.config.Configuration;

public interface Loader<T> {

    /**
     * Load object from configuration
     *
     * @param config configuration file
     * @param path   path to object
     * @return Java object
     */
    T load(Configuration config, String path);

    /**
     * Save java object to configuration
     *
     * @param object java object to save
     * @param config configuration file
     * @param path   path to object
     */
    void save(T object, Configuration config, String path);
}
