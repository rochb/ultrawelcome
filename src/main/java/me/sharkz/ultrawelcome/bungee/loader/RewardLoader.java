package me.sharkz.ultrawelcome.bungee.loader;

import me.sharkz.ultrawelcome.bungee.rewards.Reward;
import me.sharkz.ultrawelcome.bungee.utils.Util;
import net.md_5.bungee.config.Configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class RewardLoader implements Loader<Reward> {

    @Override
    public Reward load(Configuration config, String path) {
        String msg = Util.getSingleOrRandom(config, path + ".message");
        int pts = config.getInt(path + ".points");
        double money = config.getDouble(path + ".money");
        String cmd = Util.getSingleOrRandom(config, path + ".command");
        String cmdType = config.getString(path + ".commandType");
        if (config.get(path + ".items") != null && config.get(path + ".items") instanceof Configuration) {
            HashMap<String, Object> items = new HashMap<>();
            Util.getConfigSectionToMap(config, items, path + ".items");
            return new Reward(msg, pts, money, cmd, cmdType, items);
        }
        return new Reward(msg, pts, money, cmd, cmdType);
    }

    @Override
    public void save(Reward object, Configuration config, String path) {
    }

    /**
     * @param config
     * @return
     */
    public List<Reward> getRewards(Configuration config) {
        if (config.get("rewards") instanceof Configuration) {
            Configuration sec = config.getSection("rewards");
            return sec.getKeys().stream().map(s -> load(config, "rewards." + s))
                    .filter(reward -> reward.hasPoints() && (reward.hasCommand() || reward.hasMoney() || reward.hasItems()))
                    .collect(Collectors.toList());
        } else
            return new ArrayList<>();
    }
}
