package me.sharkz.ultrawelcome.bungee.loader;

import me.sharkz.ultrawelcome.bungee.rewards.Actions;
import me.sharkz.ultrawelcome.bungee.rewards.actions.*;
import me.sharkz.ultrawelcome.bungee.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.md_5.bungee.config.Configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ActionsLoader implements Loader<Actions> {

    @Override
    public Actions load(Configuration config, String path) {
        return null;
    }

    public Actions load(Configuration config, String path, boolean welcomeActions) {
        // Load broadcasts
        List<Broadcast> broadcasts = new ArrayList<>();
        if (Util.isSection(config, path + ".broadcast")) {
            if (config.getBoolean(path + ".broadcast.enabled", false)) {
                // Get action type (WelcomeActions have sender option)
                ActionType broadcastType;
                if (welcomeActions) {
                    broadcastType = ActionType.valueOf(config.getString(path + ".broadcast.type", "global").toUpperCase()) == ActionType.GLOBAL ?
                            (config.getString(path + ".broadcast.sender", "broadcast").equalsIgnoreCase("broadcast") ?
                                    ActionType.GLOBAL_BC : ActionType.GLOBAL_CHAT) :
                            (config.getString(path + ".broadcast.sender", "broadcast").equalsIgnoreCase("broadcast") ?
                                    ActionType.SINGLE_BC : ActionType.SINGLE_CHAT);
                } else {
                    broadcastType = ActionType.valueOf(config.getString(path + ".broadcast.type", "global").toUpperCase());
                }

                // Get single or multiple broadcasts
                if (Util.isSet(config, path + ".broadcast.broadcast")) {
                    String broadcast = CommonUtils.color(Util.getSingleOrRandom(config, path + ".broadcast.broadcast"));
                    broadcasts.add(new Broadcast(broadcastType, broadcast, null));
                } else if (Util.isSection(config, path + ".broadcast.broadcasts")) {
                    config.getSection(path + ".broadcast.broadcasts").getKeys().forEach(i -> {
                        String _path = path + ".broadcast.broadcasts." + i;
                        String broadcast = CommonUtils.color(Util.getSingleOrRandom(config, _path + ".message"));
                        String permission = Util.getPermission(config, _path + ".permission");
                        broadcasts.add(new Broadcast(broadcastType, broadcast, permission));
                    });
                }
            }
        }

        // Load firework
        List<Firework> fireworks = new ArrayList<>();
        if (Util.isSection(config, path + ".firework")) {
            if (config.getBoolean(path + ".firework.enabled", false)) {
                // Get action type
                ActionType fireworkType = ActionType.valueOf(config.getString(path + ".firework.type", "global").toUpperCase());

                // Get single or multiple fireworks
                if (Util.isSet(config, path + ".firework.firework")) {
                    Configuration fw = new Configuration();
                    fw.set("material", "FIREWORK_ROCKET");
                    fw.set("power", config.getInt(path + ".firework.firework.power", 1));
                    fw.set("firework.1", config.getSection(path + ".firework.firework"));
                    HashMap<String, Object> paths = new HashMap<>();
                    Util.getConfigSectionToMap(fw, paths);

                    fireworks.add(new Firework(fireworkType, paths, null));
                } else if (Util.isSection(config, path + ".firework.fireworks")) {
                    config.getSection(path + ".firework.fireworks").getKeys().forEach(i -> {
                        String _path = path + ".firework.fireworks." + i;
                        Configuration fw = new Configuration();
                        fw.set("material", "FIREWORK_ROCKET");
                        fw.set("power", config.getInt(_path + ".firework.power", 1));
                        fw.set("firework.1", config.getSection(_path + ".firework"));
                        HashMap<String, Object> paths = new HashMap<>();
                        Util.getConfigSectionToMap(fw, paths);
                        String permission = Util.getPermission(config, _path + "_permission");

                        fireworks.add(new Firework(fireworkType, paths, permission));
                    });
                }
            }
        }

        // Load commands
        List<Command> commands = new ArrayList<>();
        if (Util.isSection(config, path + ".command")) {
            if (config.getBoolean(path + ".command.enabled", false)) {
                // Get action type
                ActionType commandType = ActionType.valueOf(config.getString(path + ".command.type", "global").toUpperCase());

                // Get single or multiple commands
                if (Util.isSet(config, path + ".command.command")) {
                    String command = Util.getSingleOrRandom(config, path + ".command.command");
                    String cmdType = config.getString(path + ".command.commandType", "bungee");
                    commands.add(new Command(commandType, command, cmdType, null));
                } else if (Util.isSection(config, path + ".command.commands")) {
                    config.getSection(path + ".command.commands").getKeys().forEach(i -> {
                        String _path = path + ".command.commands." + i;
                        String command = Util.getSingleOrRandom(config, _path + ".command");
                        String cmdType = config.getString(_path + ".commandType", "bungee");
                        String permission = Util.getPermission(config, _path + ".permission");
                        commands.add(new Command(commandType, command, cmdType, permission));
                    });
                }
            }
        }

        // Load titles
        List<Title> titles = new ArrayList<>();
        if (Util.isSection(config, path + ".title")) {
            if (config.getBoolean(path + ".title.enabled", false)) {
                // Get action type
                ActionType titleType = ActionType.valueOf(config.getString(path + ".title.type", "global").toUpperCase());

                // Get single or multiple titles
                if (Util.isSection(config, path + ".title.title")) {
                    String _path = path + ".title.title";
                    String title = Util.getSingleOrRandom(config, _path + ".title");
                    String subtitle = Util.getSingleOrRandom(config, _path + ".subtitle");
                    int fade_in = config.getInt(_path + ".fade-in", 10);
                    int stay = config.getInt(_path + ".stay", 20);
                    int fade_out = config.getInt(_path + ".fade-out", 10);
                    titles.add(new Title(titleType, title, subtitle, fade_in, stay, fade_out, null));
                } else if (Util.isSection(config, path + ".title.titles")) {
                    config.getSection(path + ".title.titles").getKeys().forEach(i -> {
                        String _path = path + ".title.titles." + i;
                        String title = Util.getSingleOrRandom(config, _path + ".title");
                        String subtitle = Util.getSingleOrRandom(config, _path + ".subtitle");
                        int fade_in = config.getInt(_path + ".fade-in", 10);
                        int stay = config.getInt(_path + ".stay", 20);
                        int fade_out = config.getInt(_path + ".fade-out", 10);
                        String permission = Util.getPermission(config, _path + ".permission");
                        titles.add(new Title(titleType, title, subtitle, fade_in, stay, fade_out, permission));
                    });
                }
            }
        }

        // Load money
        List<Money> monies = new ArrayList<>();
        if (welcomeActions && Util.isSection(config, path + ".reward")) {
            if (config.getBoolean(path + ".reward.enabled", false)) {
                // Get single or multiple money
                if (Util.isSection(config, path + ".reward.reward")) {
                    String _path = path + ".reward.reward";
                    int money = config.getInt(_path + ".money", 0);
                    String message = CommonUtils.color(Util.getSingleOrRandom(config, _path + ".message"));
                    monies.add(new Money(money, message, null));
                } else if (Util.isSection(config, path + ".reward.rewards")) {
                    config.getSection(path + ".reward.rewards").getKeys().forEach(i -> {
                        String _path = path + ".reward.rewards." + i;
                        int money = config.getInt(_path + ".money", 0);
                        String message = CommonUtils.color(Util.getSingleOrRandom(config, _path + ".message"));
                        String permission = Util.getPermission(config, _path + ".permission");
                        monies.add(new Money(money, message, permission));
                    });
                }
            }
        }

        return new Actions(broadcasts, fireworks, commands, titles, monies);
    }

    @Override
    public void save(Actions object, Configuration config, String path) {

    }
}
