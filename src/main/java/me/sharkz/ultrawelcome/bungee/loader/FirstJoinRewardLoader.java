package me.sharkz.ultrawelcome.bungee.loader;

import me.sharkz.ultrawelcome.bungee.rewards.Reward;
import me.sharkz.ultrawelcome.bungee.utils.Util;
import net.md_5.bungee.config.Configuration;

import java.util.*;
import java.util.stream.Collectors;

public class FirstJoinRewardLoader implements Loader<Reward> {

    @Override
    public Reward load(Configuration config, String path) {
        String msg = Util.getSingleOrRandom(config, path + ".message");
        double money = config.getDouble(path + ".money");
        String cmd = Util.getSingleOrRandom(config, path + ".command");
        String cmdType = config.getString(path + ".commandType");
        if (config.get(path + ".items") != null && config.get(path + ".items") instanceof Configuration) {
            HashMap<String, Object> items = new HashMap<>();
            Util.getConfigSectionToMap(config, items, path + ".items");
            return new Reward(msg, 0, money, cmd, cmdType, items);
        }
        return new Reward(msg, 0, money, cmd, cmdType);
    }

    @Override
    public void save(Reward object, Configuration config, String path) {
    }

    /**
     * @param config
     * @return
     */
    public List<Reward> getRewards(Configuration config) {
        if (config.get("first-join-rewards") instanceof Configuration) {
            String firstJoinRewardType = config.getString("first-join-rewards.type", "single");
            Configuration sec = config.getSection("first-join-rewards");
            List<String> rewardsKeys = new ArrayList<>(sec.getKeys());
            rewardsKeys.remove("enabled");
            rewardsKeys.remove("type");
            switch (firstJoinRewardType) {
                case "single": {
                    return Collections.singletonList(load(config, rewardsKeys.get(0)));
                }
                case "all": {
                    return rewardsKeys.stream().map(s -> load(config, s)).collect(Collectors.toList());
                }
                case "random": {
                    return Collections.singletonList(load(config, rewardsKeys.get(new Random().nextInt(rewardsKeys.size()))));
                }
            }
        }
        return new ArrayList<>();
    }
}
