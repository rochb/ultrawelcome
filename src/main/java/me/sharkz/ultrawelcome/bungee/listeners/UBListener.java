package me.sharkz.ultrawelcome.bungee.listeners;

import me.sharkz.ultrawelcome.bungee.UWBungee;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class UBListener implements Listener {

    @EventHandler
    public void onJoin(PostLoginEvent e) {
        ProxiedPlayer player = e.getPlayer();
		
        if (player.hasPermission("ultrawelcome.reload") && UWBungee.I.newVersion && UWBungee.I.getConfiguration().isNotifyUpdateOnJoin()) {
            player.sendMessage(CommonUtils.newVersionMessage());
        }
    }

}
