package me.sharkz.ultrawelcome.bukkit.utils;

import me.clip.placeholderapi.PlaceholderAPI;
import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Util {

    /**
     * @param sender
     * @param msg
     */
    public static void sendMessage(CommandSender sender, Lang msg) {
        sendMessage(sender, msg.toString(), new HashMap<>(), new HashMap<>());
    }

    public static void sendMessage(CommandSender sender, String msg) {
        sendMessage(sender, msg, new HashMap<>(), new HashMap<>());
    }

    public static void sendMessage(CommandSender sender, Lang msg, Map<String, String> placeholders) {
        sendMessage(sender, msg.toString(), placeholders, new HashMap<>());
    }

    /**
     * @param sender
     * @param msg
     * @param placeholders
     */
    public static void sendMessage(CommandSender sender, String msg, Map<String, String> placeholders, Map<String, Player> perPlayerPlaceholders) {
        List<String> tmp = new ArrayList<>();

        tmp.add(msg);

        // CUSTOM PLACEHOLDERS
        if (placeholders == null) placeholders = new HashMap<>();
        placeholders.put("%prefix%", Lang.PREFIX.toString());
        placeholders.put("%player%", sender.getName());
        for (String key : placeholders.keySet()) {
            Map<String, String> finalPlaceholders = placeholders;
            tmp = tmp.stream().map(s -> s.replaceAll(key, finalPlaceholders.get(key))).collect(Collectors.toList());
        }

        // PAPI
        if (UW.PAPIEnabled && sender instanceof Player)
            tmp = tmp.stream()
                    .map(s -> applyPlaceholders(s, new HashMap<>(), (Player) sender, perPlayerPlaceholders))
                    .collect(Collectors.toList());

        // COLOR
        tmp = tmp.stream()
                .map(CommonUtils::color)
                .collect(Collectors.toList());

        // SEND
        tmp.forEach(sender::sendMessage);
    }

    /**
     * Get if a player inventory is full.
     *
     * @param player
     * @return
     */
    public static boolean isFull(Player player) {
        return Arrays.stream(player.getInventory().getContents()).noneMatch(Objects::isNull);
    }

    /**
     * Returns a one-line or random String from the config by a given path.
     * If the path is a String it means that it is a one-line string.
     * Otherwise, if it is a List it means that it is a list of random
     * strings so it returns a random string from the list.
     *
     * @param config Configuration file
     * @param path Path of the value to get
     * @return Value got from the config
     */
    public static String getSingleOrRandom(FileConfiguration config, String path) {
        String s = "";
        if (config.isString(path)) {
            s = config.getString(path);
        } else if (config.isList(path)) {
            List<String> randomS = config.getStringList(path);
            s = randomS.get(new Random().nextInt(randomS.size()));
        }
        return s;
    }

    /**
     * Returns a list of the names of the currently online players on the server.
     *
     * @return List of online players names
     */
    public static List<String> getOnlinePlayerNames() {
        return getPlayerNames(Bukkit.getOnlinePlayers());
    }

    /**
     * Returns a list of the names of the players in the given list.
     *
     * @param players List of players where to get the names
     * @return List of players names
     */
    public static List<String> getPlayerNames(Collection<? extends Player> players) {
        List<String> l = new ArrayList<>(players.size());
        for (Player p : players) {
            l.add(p.getName());
        }
        return l;
    }

    /**
     * Applies placeholders to a given string.
     *
     * @param string String for placeholders ot be applied
     * @param placeholders Map of strings to be replaced with other strings
     * @param player Main player for PAPI to retrieve placeholders
     * @param perPlayerPlaceholders Map of placeholders corresponding to players for multiple players placeholders
     * @return String with applied placeholders
     */
    public static String applyPlaceholders(String string, Map<String, String> placeholders, Player player, Map<String, Player> perPlayerPlaceholders) {
        // Defined placeholders
        String finalS = string;
        for (String k : placeholders.keySet()) {
            finalS = finalS.replaceAll(k, placeholders.get(k));
        }

        // PAPI
        if (UW.PAPIEnabled) {
            /*
             * Enables the possibility to apply placeholders for multiple players.
             * The regex search for something like this: "%new_player%%luckperms_group%Steve"
             * If it finds "%new_player%" it gets the PAPI placeholder next to it,
             * retrieves the final product and puts it into the string
             *
             */
            for (String k : perPlayerPlaceholders.keySet()) {
                Matcher m = Pattern.compile("(" + k + "(%.+?%))").matcher(finalS);
                while (m.find()) {
                    finalS = finalS.replace(m.group(1), PlaceholderAPI.setPlaceholders(perPlayerPlaceholders.get(k), m.group(2)));
                }
            }

            // All the remaining placeholders are changed using the specified default player
            if (player != null) finalS = PlaceholderAPI.setPlaceholders(player, finalS);
        }

        return finalS;
    }

    /**
     * Get bukkit permission from config.
     *
     * @param config Configuration file from where to get data
     * @param path Path of the permission in the config
     * @return Permission if it founds it else null
     */
    public static Permission getPermission(FileConfiguration config, String path) {
        return config.isSet(path) ?
                config.getString(path).isEmpty() ?
                        null : new Permission(config.getString(path))
                : null;
    }
}
