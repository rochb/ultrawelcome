package me.sharkz.ultrawelcome.bukkit.utils;

import com.cryptomorin.xseries.XItemStack;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import me.clip.placeholderapi.PlaceholderAPI;
import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BukkitChannel implements PluginMessageListener {

    public static YamlConfiguration playersData = new YamlConfiguration();
    public static String playersStorageType = null;
    public static String latestPlayer = null;

    public static void send(String channel, Player player, String string) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(channel);
        out.writeUTF(player.getName());
        out.writeUTF(string);

        player.sendPluginMessage(UW.I, CommonUtils.CHANNEL, out.toByteArray());
    }

    public static void send(String channel, Player player, boolean bool) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(channel);
        out.writeUTF(player.getName());
        out.writeBoolean(bool);

        player.sendPluginMessage(UW.I, CommonUtils.CHANNEL, out.toByteArray());
    }

    public static void sendPlaceholder(Player player, String id, String s, String s1) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("PAPI");
        out.writeUTF(player.getName());
        out.writeUTF(id);
        out.writeUTF(s);
        out.writeUTF(s1);

        player.sendPluginMessage(UW.I, CommonUtils.CHANNEL, out.toByteArray());
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] bytes) {
        if (!channel.equalsIgnoreCase(CommonUtils.CHANNEL)) return;

        ByteArrayDataInput in = ByteStreams.newDataInput(bytes);

        String subChannel = in.readUTF();
        Player p = Bukkit.getPlayer(in.readUTF());
        switch (subChannel) {
            case "items": {
                // Config where to put incoming paths from bungeecord
                YamlConfiguration bungeedata = new YamlConfiguration();

                // Deserialize incoming object from bungeecord containing all paths for the rewards items
                HashMap<String, Object> itemsPaths = null;
                try {
                    itemsPaths = (HashMap<String, Object>) CommonUtils.deserialize(in.readUTF());
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                itemsPaths.forEach(bungeedata::set);

                // Load rewards from the config
                List<ItemStack> items = new ArrayList<>();
                bungeedata.getKeys(false).forEach(path1 -> bungeedata.getConfigurationSection(path1).getKeys(false).forEach(s -> {
                    String path = path1 + "." + s;
                    bungeedata.getConfigurationSection(path + ".items").getKeys(true).forEach(s1 -> {
                        if (bungeedata.isConfigurationSection(s + ".items." + s1)) {
                            ItemStack i = XItemStack.deserialize(bungeedata.getConfigurationSection(path + ".items." + s1));
                            if (i != null) items.add(i);
                        }
                    });
                }));

                // Give rewards items to the rewarded player
                items.forEach(itemStack -> {
                    if (Util.isFull(player)) {
                        player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
                    } else {
                        player.getInventory().addItem(itemStack);
                    }
                });

                break;
            }
            case "money": {
                // Give incoming money from bungeecord to the player
                double money = in.readInt();
                if (UW.vaultEnabled)
                    UW.I.economy.depositPlayer(p, money);
                break;
            }
            case "command": {
                // Execute incoming message from bungeecord
                String cmd = in.readUTF();
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
                break;
            }
            case "PAPI": {
                String id = in.readUTF();
                String s = in.readUTF();
                String s1 = in.readUTF();
                String s2 = PlaceholderAPI.setPlaceholders(player, s1);
                sendPlaceholder(player, id, s, s2);
                break;
            }
            case "Broadcast": {
                String message = in.readUTF();
                UW.I.broadcasts.put(player.getUniqueId(), null);
                player.chat(message);
                break;
            }
            case "PlayersData": {
                String s = in.readUTF();
                int points = in.readInt();
                int number = in.readInt();
                playersData.set(s + ".points", points);
                playersData.set(s + ".number", number);
                break;
            }
            case "PlayersStorage": {
                playersStorageType = in.readUTF();
                break;
            }
            case "PlayersDataUpdate": {
                String s = in.readUTF();
                int points = in.readInt();
                playersData.set(s + ".points", points);
                break;
            }
            case "LatestPlayer": {
                latestPlayer = in.readUTF();
                break;
            }
        }
    }

    public static String getPlayerDataPath(OfflinePlayer player) {
        if (playersStorageType.equals("uuid")) {
            return player.getUniqueId().toString();
        }
        return player.getName();
    }
}
