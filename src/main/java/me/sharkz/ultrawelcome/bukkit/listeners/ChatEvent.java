package me.sharkz.ultrawelcome.bukkit.listeners;

import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.bukkit.rewards.actions.Broadcast;
import me.sharkz.ultrawelcome.bukkit.utils.BukkitChannel;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatEvent implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent e) {
        if (!UW.I.broadcasts.containsKey(e.getPlayer().getUniqueId())) return;
        UW.I.broadcasts.remove(e.getPlayer().getUniqueId());
        if (UW.I.getConfiguration().isBungeecord()) {
            e.setFormat(e.getFormat());
            BukkitChannel.send("Broadcast", e.getPlayer(), e.getFormat());
            e.setCancelled(true);
        } else {
            Broadcast broadcast = UW.I.broadcasts.get(e.getPlayer().getUniqueId());
            broadcast.getReceivers().forEach(e.getRecipients()::remove);
            e.getRecipients().forEach(r -> {
                if (!broadcast.getReceivers().contains(r)) e.getRecipients().remove(r);
            });
        }
    }
}
