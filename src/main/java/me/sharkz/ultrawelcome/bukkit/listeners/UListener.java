package me.sharkz.ultrawelcome.bukkit.listeners;

import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.bukkit.commands.WelcomeCmd;
import me.sharkz.ultrawelcome.bukkit.utils.BukkitChannel;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collections;

public class UListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();

        if (UW.I.getConfiguration().isBungeecord()) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    BukkitChannel.send("State", player, UW.PAPIEnabled);
                }
            }.runTaskLater(UW.I, 5L);
        } else {
            if (player.hasPermission("ultrawelcome.reload") && UW.newVersion && UW.I.getConfiguration().isNotifyUpdateOnJoin()) {
                player.spigot().sendMessage(CommonUtils.newVersionMessage());
            }
            if (!UW.I.getPlayers().hasPlayedBefore(player)) {
                if (WelcomeCmd.welcoming == null) {
                    WelcomeCmd.welcoming = player.getUniqueId();
                    WelcomeCmd.welcomers.clear();
                }
                WelcomeCmd.newestPlayer = player.getUniqueId();
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (WelcomeCmd.newestPlayer.equals(player.getUniqueId())) WelcomeCmd.welcoming = null;
                    }
                }.runTaskLater(UW.I, WelcomeCmd.time_limit * 20L);

                UW.I.getPlayers().addPlayer(player);

                if (UW.I.getConfiguration().isFirstJoinRewards()) {
                    UW.I.getConfiguration().getFirstJoinRewards().forEach(reward -> reward.giveReward(player));
                }
                UW.I.getConfiguration().getFirstJoinActions().execute(player, null, Collections.singletonMap("new_player", player));
            } else {
                UW.I.getConfiguration().getJoinActions().execute(player, null, Collections.singletonMap("joined_player", player));
            }
        }
    }
}
