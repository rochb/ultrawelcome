package me.sharkz.ultrawelcome.bukkit.commands;

import me.sharkz.ultrawelcome.bukkit.UW;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginIdentifiableCommand;

import java.lang.reflect.Field;
import java.util.List;

public abstract class UCmd extends Command implements CommandExecutor, PluginIdentifiableCommand {

    private static CommandMap commandMap;

    static {
        try {
            Field f = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            f.setAccessible(true);
            commandMap = (CommandMap) f.get(Bukkit.getServer());
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private boolean register = false;

    public UCmd(String name, String description, String usageMessage, List<String> aliases) {
        super(name, description, usageMessage, aliases);
        registerCommand();
    }

    /**
     * Register the command
     *
     */
    public void registerCommand() {
        if (!register)
            register = commandMap.register(UW.I.getName(), this);
    }
}
