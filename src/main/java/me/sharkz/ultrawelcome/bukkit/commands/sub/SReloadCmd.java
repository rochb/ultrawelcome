package me.sharkz.ultrawelcome.bukkit.commands.sub;

import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.bukkit.utils.Lang;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class SReloadCmd implements ISubCmd {

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!sender.hasPermission("ultrawelcome.reload")) {
            Util.sendMessage(sender, Lang.NO_PERMISSION);
            return;
        }

        UW.I.getConfiguration().reload();
        UW.I.loadLang();
        Util.sendMessage(sender, Lang.RELOADED);
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        return new ArrayList<>();
    }
}
