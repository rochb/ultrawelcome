package me.sharkz.ultrawelcome.bukkit.commands;

import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.bukkit.utils.Lang;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.Plugin;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class WelcomeCmd extends UCmd {

    public static UUID newestPlayer;
    public static UUID welcoming;
    public static final List<UUID> welcomers = new ArrayList<>();
    public static int time_limit = 12;
    private final Permission permission;

    public WelcomeCmd(String name, Permission permission, List<String> aliases, int time) {
        super(name, "Say welcome to new players", "/" + name, aliases);
        time_limit = time;
        this.permission = permission;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return true;
    }

    @Override
    public Plugin getPlugin() {
        return UW.I;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (args.length == 1) CommonUtils.filterTabCompleteOptions(Collections.singletonList("top"));
        return new ArrayList<>();
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        FileConfiguration config = UW.I.getConfiguration().getConfig();
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("top")) {
                List<String> header = new ArrayList<>();
                if (config.isList("top.header")) header = config.getStringList("top.header");
                else header.add(config.getString("top.header", "&7Top welcomers : "));
                header.forEach(s -> Util.sendMessage(sender, s));
                Map<String, Integer> tmp = UW.I.getPlayers().getPoints();
                if (tmp.size() == 0)
                    Util.sendMessage(sender, Lang.ANYTHING_TO_DISPLAY);
                else {
                    AtomicInteger rank = new AtomicInteger(1);
                    tmp.forEach((s, integer) -> {
                        if (rank.get() <= config.getInt("top.length", 10)) {
                            Util.sendMessage(sender,
                                    config.getString("top.format", "&7N°&a%rank% - &a%rankedPlayer%&7 &b%points%&7 points"),
                                    topSubCommandPlaceholders(rank, s, integer),
                                    sender instanceof Player ? Collections.singletonMap("ph_rankedPlayer", (Player) sender) : new HashMap<>()
                            );
                        }
                    });
                }
            } else if (args[0].equalsIgnoreCase("help")) {
                Util.sendMessage(sender, Lang.WELCOME_COMMAND_HELP);
            }
        } else {
            // Console check
            if (!(sender instanceof Player)) {
                Util.sendMessage(sender, Lang.NOT_A_PLAYER);
                return true;
            }
            Player player = (Player) sender;
            // Permission check
            if (permission != null && !player.hasPermission(permission)) {
                Util.sendMessage(sender, Lang.NO_PERMISSION);
                return true;
            }
            // New player check
            if (welcoming == null) {
                Util.sendMessage(player, Lang.NO_NEW_PLAYER);
                return true;
            }
            // Target check
            Player target = Bukkit.getPlayer(newestPlayer);
            if (target == null || !target.isOnline()) {
                Util.sendMessage(player, Lang.OFFLINE_PLAYER);
                return true;
            }
            // Self check
            if (newestPlayer.equals(player.getUniqueId())) {
                Util.sendMessage(player, Lang.WELCOME_SELF);
                return true;
            }
            // Variable reset
            if (!newestPlayer.equals(welcoming)) {
                welcoming = UUID.fromString(newestPlayer.toString());
                welcomers.clear();
            }

            // Already welcome check
            if (welcomers.contains(player.getUniqueId())) {
                Util.sendMessage(player, Lang.ALREADY_WELCOME);
                return true;
            }

            Map<String, Player> placeholders = new HashMap<>();
            placeholders.put("player", player);
            placeholders.put("new_player", target);
            UW.I.getConfiguration().getWelcomeActions().execute(player, target, placeholders);

            // Add points to player
            UW.I.getPlayers().addPoints(player, 1);

            // Rewards
            UW.I.getConfiguration().getMilestones().stream().filter(reward -> reward.getPoints() == UW.I.getPlayers().getPoints(player)).forEach(reward -> reward.giveReward(player));

            welcomers.add(player.getUniqueId());
        }
        return false;
    }

    private Map<String, String> topSubCommandPlaceholders(AtomicInteger rank, String s, int integer) {
        Map<String, String> map = new HashMap<>();
        map.put("%rank%", String.valueOf(rank.getAndIncrement()));
        map.put("%points%", String.valueOf(integer));
        map.put("%rankedPlayer%", s);
        return map;
    }
}
