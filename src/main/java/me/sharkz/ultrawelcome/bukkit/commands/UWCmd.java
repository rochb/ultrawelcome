package me.sharkz.ultrawelcome.bukkit.commands;

import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.bukkit.commands.sub.ISubCmd;
import me.sharkz.ultrawelcome.bukkit.commands.sub.SPointsCmd;
import me.sharkz.ultrawelcome.bukkit.commands.sub.SReloadCmd;
import me.sharkz.ultrawelcome.bukkit.utils.Lang;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.util.*;

public class UWCmd extends UCmd {

    private final HashMap<String, ISubCmd> subCommands = new HashMap<>();

    public UWCmd() {
        super("ultrawelcome", "Secret command for admins :)", "/uw", Collections.singletonList("uw"));

        subCommands.put("reload", new SReloadCmd());
        subCommands.put("points", new SPointsCmd());
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (args.length == 0) {
            Util.sendMessage(sender, "%prefix% &7Version: &a&l" + UW.I.getDescription().getVersion());
            return true;
        }

        ISubCmd subCmd = subCommands.get(args[0]);
        if (subCmd == null) {
            Util.sendMessage(sender, Lang.INVALID_SUBCOMMAND);
            return true;
        }
        subCmd.execute(sender, Arrays.copyOfRange(args, 1, args.length));
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (args.length == 1) {
            return CommonUtils.filterTabCompleteOptions(subCommands.keySet(), args);
        }

        ISubCmd subCmd = subCommands.get(args[0]);
        if (subCmd == null) return new ArrayList<>();
        return subCmd.tabComplete(sender, Arrays.copyOfRange(args, 1, args.length));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return true;
    }

    @Override
    public Plugin getPlugin() {
        return UW.I;
    }
}
