package me.sharkz.ultrawelcome.bukkit.commands.sub;

import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.bukkit.utils.Lang;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

public class SPointsCmd implements ISubCmd {

    private final List<String> subArgs = Arrays.asList("get", "add", "set", "remove");

    @Override
    public void execute(CommandSender sender, String[] args) {
        // Permission
        if (!sender.hasPermission("ultrawelcome.points")) {
            Util.sendMessage(sender, Lang.NO_PERMISSION);
            return;
        }
        // Length
        if (args.length < 2) {
            Util.sendMessage(sender, Lang.POINTS_COMMAND_HELP);
            return;
        }
        // Correct sub argument
        if (!subArgs.contains(args[0].toLowerCase())) {
            Util.sendMessage(sender, Lang.POINTS_COMMAND_HELP);
            return;
        }
        // Valid target
        Player target = Bukkit.getPlayerExact(args[1]);
        if (target == null) {
            Util.sendMessage(sender, Lang.OFFLINE_PLAYER);
            return;
        }

        Map<String, String> placeholders = new HashMap<>();
        placeholders.put("%player%", target.getName());

        int currentUserPoints = UW.I.getPlayers().getPoints(target);

        if (args[0].equalsIgnoreCase("get")) {
            placeholders.put("%points%", String.valueOf(currentUserPoints));
            Util.sendMessage(sender, Lang.POINTS_GET, placeholders);
            return;
        }

        // Length
        if (args.length < 3) {
            Util.sendMessage(sender, Lang.POINTS_COMMAND_HELP);
            return;
        }
        // Valid number of points
        if (!CommonUtils.isNumeric(args[2])) {
            Util.sendMessage(sender, Lang.INVALID_NUMBER);
            return;
        }

        int points = Integer.parseInt(args[2]);
        placeholders.put("%points%", String.valueOf(points));
        placeholders.put("%current_points%", String.valueOf(currentUserPoints));

        switch (args[0].toLowerCase()) {
            case "add": {
                UW.I.getPlayers().addPoints(target, points);
                Util.sendMessage(sender, Lang.POINTS_ADDED, placeholders);
                break;
            }
            case "set": {
                UW.I.getPlayers().setPoints(target, points);
                Util.sendMessage(sender, Lang.POINTS_SET, placeholders);
                break;
            }
            case "remove": {
                UW.I.getPlayers().removePoints(target, points);
                Util.sendMessage(sender, Lang.POINTS_REMOVED, placeholders);
                break;
            }
        }
        if (args.length > 3 && points != 0 && !args[0].equalsIgnoreCase("remove")) {
            boolean giveRewards = Boolean.parseBoolean(args[3]);
            if (giveRewards) {
                UW.I.getConfiguration().getMilestones().stream().filter(reward -> reward.getPoints() <= UW.I.getPlayers().getPoints(target) && reward.getPoints() > currentUserPoints)
                        .forEach(reward -> reward.giveReward(target));
            }
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        if (args.length == 1) return CommonUtils.filterTabCompleteOptions(subArgs, args);
        if (args.length == 2) return CommonUtils.filterTabCompleteOptions(Util.getOnlinePlayerNames(), args);
        if (args.length == 4) return CommonUtils.filterTabCompleteOptions(Arrays.asList("true", "false"), args);

        return new ArrayList<>();
    }
}
