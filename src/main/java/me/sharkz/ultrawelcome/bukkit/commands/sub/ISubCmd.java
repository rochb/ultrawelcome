package me.sharkz.ultrawelcome.bukkit.commands.sub;

import org.bukkit.command.CommandSender;

import java.util.List;

public interface ISubCmd {

    void execute(CommandSender sender, String[] args);

    List<String> tabComplete(CommandSender sender, String[] args);

}
