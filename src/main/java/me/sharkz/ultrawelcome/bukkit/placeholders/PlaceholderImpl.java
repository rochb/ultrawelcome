package me.sharkz.ultrawelcome.bukkit.placeholders;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.bukkit.commands.WelcomeCmd;
import me.sharkz.ultrawelcome.bukkit.utils.BukkitChannel;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;


public class PlaceholderImpl extends PlaceholderExpansion {

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getIdentifier() {
        return "ultrawelcome";
    }

    @Override
    public String getAuthor() {
        return "Sharkz";
    }

    @Override
    public String getVersion() {
        return UW.I.getDescription().getVersion();
    }

    @Override
    public String onRequest(OfflinePlayer p, String id) {
        if (p == null) return "";

        if (UW.I.getConfiguration().isBungeecord()) {
            if (id.equalsIgnoreCase("latestPlayer")) {
                return BukkitChannel.latestPlayer != null ? BukkitChannel.latestPlayer : "No new player";
            } else if (id.equalsIgnoreCase("points")) {
                return String.valueOf(BukkitChannel.playersData.get(BukkitChannel.getPlayerDataPath(p) + ".points"));
            } else if (id.equalsIgnoreCase("number")) {
                return String.valueOf(BukkitChannel.playersData.get(BukkitChannel.getPlayerDataPath(p) + ".number"));
            }
        } else {
            if (id.equalsIgnoreCase("latestPlayer")) {
                return WelcomeCmd.newestPlayer != null ? Bukkit.getOfflinePlayer(WelcomeCmd.newestPlayer).getName() : "No new player";
            } else if (id.equalsIgnoreCase("points")) {
                return String.valueOf(UW.I.getPlayers().getPoints(p));
            } else if (id.equalsIgnoreCase("number")) {
                return String.valueOf(UW.I.getPlayers().getPlayerNumber(p));
            }
        }
        return "";
    }
}
