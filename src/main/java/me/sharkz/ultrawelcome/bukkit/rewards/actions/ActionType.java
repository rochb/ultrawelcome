package me.sharkz.ultrawelcome.bukkit.rewards.actions;

public enum ActionType {

    GLOBAL,
    SINGLE,
    GLOBAL_BC,
    SINGLE_BC,
    GLOBAL_CHAT,
    SINGLE_CHAT

}
