package me.sharkz.ultrawelcome.bukkit.rewards.actions;

import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Broadcast {

    private final ActionType actionType;
    private final String content;
    private List<Player> receivers;
    private final Permission permission;

    public Broadcast(ActionType actionType, String content, Permission permission) {
        this.actionType = actionType;
        this.content = content;
        this.permission = permission;
    }

    public String getContent() {
        return content;
    }

    public List<Player> getReceivers() {
        return receivers;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setReceivers(List<Player> receivers) {
        this.receivers = receivers;
    }

    public void run(Player sender, Player newPlayer, Map<String, String> placeholders, Map<String, Player> perPlayerPlaceholders) {
        String bc = CommonUtils.color(Util.applyPlaceholders(content, placeholders, sender, perPlayerPlaceholders));
        switch (actionType) {
            case GLOBAL:
            case GLOBAL_BC: {
                Bukkit.getOnlinePlayers().forEach(player -> {
                    if (permission == null || player.hasPermission(permission)) player.sendMessage(bc);
                });
                break;
            }
            case SINGLE:
            case SINGLE_BC: {
                if (sender != null && (permission == null || sender.hasPermission(permission))) sender.sendMessage(bc);
                if (newPlayer != null && (permission == null || newPlayer.hasPermission(permission))) newPlayer.sendMessage(bc);
                break;
            }
            case GLOBAL_CHAT: {
                if (sender != null) {
                    setReceivers(permission == null ? new ArrayList<>(Bukkit.getOnlinePlayers()) : Bukkit.getOnlinePlayers().stream().filter(player -> player.hasPermission(permission)).collect(Collectors.toList()));
                    UW.I.broadcasts.put(sender.getUniqueId(), this);
                    sender.chat(bc);
                }
                break;
            }
            case SINGLE_CHAT: {
                List<Player> rc = new ArrayList<>();
                if (sender != null) {
                    if (permission == null || sender.hasPermission(permission)) rc.add(sender);
                    if (newPlayer != null && (permission == null || newPlayer.hasPermission(permission)))
                        rc.add(newPlayer);
                    setReceivers(rc);
                    UW.I.broadcasts.put(sender.getUniqueId(), this);
                    sender.chat(bc);
                }
                break;
            }
        }
    }
}
