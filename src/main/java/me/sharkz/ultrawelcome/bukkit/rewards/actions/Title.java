package me.sharkz.ultrawelcome.bukkit.rewards.actions;

import com.cryptomorin.xseries.messages.Titles;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import java.util.Map;

public class Title {

    private final ActionType actionType;
    private final String title;
    private final String subtitle;
    private final int fadeIn;
    private final int stay;
    private final int fadeOut;
    private final Permission permission;

    public Title(ActionType actionType, String title, String subtitle, int fadeIn, int stay, int fadeOut, Permission permission) {
        this.actionType = actionType;
        this.title = title;
        this.subtitle = subtitle;
        this.fadeIn = fadeIn;
        this.stay = stay;
        this.fadeOut = fadeOut;
        this.permission = permission;
    }

    public void run(Player sender, Player newPlayer, Map<String, String> placeholders, Map<String, Player> perPlayerPlaceholders) {
        String finalTitle = CommonUtils.color(Util.applyPlaceholders(title, placeholders, sender, perPlayerPlaceholders));
        String finalSubtitle = CommonUtils.color(Util.applyPlaceholders(subtitle, placeholders, sender, perPlayerPlaceholders));
        switch (actionType) {
            case GLOBAL: {
                Bukkit.getOnlinePlayers().forEach(player -> {
                    if (permission == null || player.hasPermission(permission)) Titles.sendTitle(player, fadeIn, stay, fadeOut, finalTitle, finalSubtitle);
                });
                break;
            }
            case SINGLE: {
                if (sender != null && (permission == null || sender.hasPermission(permission))) Titles.sendTitle(sender, fadeIn, stay, fadeOut, finalTitle, finalSubtitle);
                if (newPlayer != null && (permission == null || newPlayer.hasPermission(permission))) Titles.sendTitle(newPlayer, fadeIn, stay, fadeOut, finalTitle, finalSubtitle);
                break;
            }
        }
    }
}
