package me.sharkz.ultrawelcome.bukkit.rewards;

import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class Reward {

    private final String message;
    private final int points;
    private final double money;
    private final String command;
    private final List<ItemStack> items;

    public Reward(String message, int points, double money, String command) {
        this.message = message;
        this.points = points;
        this.money = money;
        this.command = command;
        this.items = new ArrayList<>();
    }

    public Reward(String message, int points, double money, String command, List<ItemStack> items) {
        this.message = message;
        this.points = points;
        this.money = money;
        this.command = command;
        this.items = items;
    }

    public boolean hasMessage() {
        return message != null && !message.isEmpty();
    }

    public boolean hasPoints() {
        return points != 0;
    }

    public boolean hasMoney() {
        return money != 0;
    }

    public boolean hasCommand() {
        return command != null && !command.isEmpty();
    }

    public boolean hasItems() {
        return items != null && items.size() > 0;
    }

    public String getMessage() {
        return message;
    }

    public int getPoints() {
        return points;
    }

    public double getMoney() {
        return money;
    }

    public String getCommand() {
        return command;
    }

    public List<ItemStack> getItems() {
        return items;
    }

    public void giveReward(Player player) {
        if (hasMoney())
            if (UW.vaultEnabled) {
                UW.I.economy.depositPlayer(player, getMoney());
            } else {
                UW.L.warning("Cannot give $" + getMoney() + " to " + player.getName() + " because Vault is not enabled !");
            }
        if (hasCommand()) {
            String cmd = getCommand();
            if (cmd.startsWith("/")) cmd = cmd.replaceFirst("/", "");
            if (cmd.contains("%player%")) cmd = cmd.replace("%player%", player.getName());
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
        }
        if (hasMessage()) {
            Util.sendMessage(player, getMessage());
        }
        if (hasItems()) {
            getItems().forEach(itemStack -> {
                if (Util.isFull(player)) {
                    player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
                } else {
                    player.getInventory().addItem(itemStack);
                }
            });
        }
    }
}
