package me.sharkz.ultrawelcome.bukkit.rewards;

import me.sharkz.ultrawelcome.bukkit.rewards.actions.*;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Actions {

    private final List<Broadcast> broadcasts;
    private final List<Firework> firework;
    private final List<Command> commands;
    private final List<Title> titles;
    private final List<Money> monies;

    public Actions(List<Broadcast> broadcasts, List<Firework> firework, List<Command> commands, List<Title> titles, List<Money> monies) {
        this.broadcasts = broadcasts;
        this.firework = firework;
        this.commands = commands;
        this.titles = titles;
        this.monies = monies;
    }

    public void execute(Player player, Player target, Map<String, Player> placeholders) {
        Map<String, String> ph = new HashMap<>();
        Map<String, Player> perPlayerPlaceholders = new HashMap<>();
        placeholders.forEach((k, v) -> {
            ph.put("%" + k + "%", v.getName());
            perPlayerPlaceholders.put("%ph_" + k + "%", v);
        });

        if (hasBroadcasts()) broadcasts.forEach(bc -> bc.run(player, target, ph, perPlayerPlaceholders));
        if (hasCommands()) commands.forEach(cmd -> cmd.run(player, target, ph));
        if (hasFirework()) firework.forEach(fw -> fw.run(player, target));
        if (hasTitles()) titles.forEach(title -> title.run(player, target, ph, perPlayerPlaceholders));
        if (hasMonies()) monies.forEach(money -> money.run(player, ph, perPlayerPlaceholders));
    }

    private boolean hasBroadcasts() {
        return !broadcasts.isEmpty();
    }

    private boolean hasFirework() {
        return !firework.isEmpty();
    }

    private boolean hasCommands() {
        return !commands.isEmpty();
    }

    private boolean hasTitles() {
        return !titles.isEmpty();
    }

    private boolean hasMonies() {
        return !monies.isEmpty();
    }

}
