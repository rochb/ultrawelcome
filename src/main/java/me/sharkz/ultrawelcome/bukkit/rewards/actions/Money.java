package me.sharkz.ultrawelcome.bukkit.rewards.actions;

import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import java.util.Map;

public class Money {

    private final int money;
    private final String message;
    private final Permission permission;

    public Money(int money, String message, Permission permission) {
        this.money = money;
        this.message = message;
        this.permission = permission;
    }

    public void run(Player sender, Map<String, String> placeholders, Map<String, Player> perPlayerPlaceholders) {
        if (sender != null && money > 0 && (permission == null || sender.hasPermission(permission))) {
            UW.I.economy.depositPlayer(sender, money);
            if (message != null && !message.isEmpty()) sender.sendMessage(CommonUtils.color(Util.applyPlaceholders(message, placeholders, sender, perPlayerPlaceholders)));
        }
    }
}
