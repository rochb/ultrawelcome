package me.sharkz.ultrawelcome.bukkit.rewards.actions;

import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.permissions.Permission;

public class Firework {

    private final ActionType actionType;
    private final FireworkMeta fireworkMeta;
    private final Permission permission;

    public Firework(ActionType actionType, FireworkMeta fireworkMeta, Permission permission) {
        this.actionType = actionType;
        this.fireworkMeta = fireworkMeta;
        this.permission = permission;
    }

    public void run(Player sender, Player newPlayer) {
        switch (actionType) {
            case GLOBAL: {
                Bukkit.getOnlinePlayers().forEach(player -> {if (permission == null || player.hasPermission(permission)) ((org.bukkit.entity.Firework) player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK)).setFireworkMeta(fireworkMeta);});
                break;
            }
            case SINGLE: {
                if (permission == null || sender.hasPermission(permission)) ((org.bukkit.entity.Firework) sender.getWorld().spawnEntity(sender.getLocation(), EntityType.FIREWORK)).setFireworkMeta(fireworkMeta);
                if (newPlayer != null && permission == null || newPlayer.hasPermission(permission)) ((org.bukkit.entity.Firework) newPlayer.getWorld().spawnEntity(newPlayer.getLocation(), EntityType.FIREWORK)).setFireworkMeta(fireworkMeta);
                break;
            }
        }
    }

}
