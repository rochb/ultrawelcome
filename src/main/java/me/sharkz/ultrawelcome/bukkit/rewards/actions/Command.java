package me.sharkz.ultrawelcome.bukkit.rewards.actions;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import java.util.Map;

public class Command {

    private final ActionType actionType;
    private final String command;
    private final Permission permission;

    public Command(ActionType actionType, String command, Permission permission) {
        this.actionType = actionType;
        this.command = command;
        this.permission = permission;
    }

    public void run(Player sender, Player newPlayer, Map<String ,String> placeholders) {
        String tempCmd = command.trim();
        if (tempCmd.startsWith("/")) tempCmd = tempCmd.substring(1);
        for (String s : placeholders.keySet()) {
            tempCmd = tempCmd.replace(s, placeholders.get(s));
        }
        String cmd = tempCmd;
        switch (actionType) {
            case GLOBAL: {
                Bukkit.getOnlinePlayers().forEach(player -> {
                    if (permission == null || player.hasPermission(permission))
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("%player%", player.getName()));
                });
                break;
            }
            case SINGLE: {
                if (permission == null || sender.hasPermission(permission)) {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
                }
                break;
            }
        }
    }
}
