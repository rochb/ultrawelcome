package me.sharkz.ultrawelcome.bukkit.files;

import com.google.common.io.ByteStreams;
import me.sharkz.ultrawelcome.bukkit.UW;
import me.sharkz.ultrawelcome.bukkit.loader.ActionsLoader;
import me.sharkz.ultrawelcome.bukkit.loader.FirstJoinRewardLoader;
import me.sharkz.ultrawelcome.bukkit.loader.RewardLoader;
import me.sharkz.ultrawelcome.bukkit.rewards.Actions;
import me.sharkz.ultrawelcome.bukkit.rewards.Reward;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.util.List;

public class Config {

    private final YamlConfiguration config = new YamlConfiguration();
    private final File file = new File(UW.I.getDataFolder(), "config.yml");

    private String lang;

    private Actions welcomeActions;
    private Actions firstJoinActions;
    private Actions joinActions;

    private List<Reward> milestones;
    private List<Reward> firstJoinRewards;

    private boolean firstJoinRewardsEnabled;

    private boolean bungeecord;
    private boolean notifyUpdateOnJoin;

    public Config() {
        if (!file.exists()) {
            try (InputStream in = UW.I.getResource("config.yml"); OutputStream os = new FileOutputStream(file)) {
                ByteStreams.copy(in, os);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        reload();
    }

    public YamlConfiguration getConfig() {
        return config;
    }

    public void reload() {
        try {
            config.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            UW.L.warning("Cannot load config.yml !");
            e.printStackTrace();
        }

        this.lang = config.getString("lang", "en_US");

        this.welcomeActions = new ActionsLoader().load(config, "onWelcome", true);
        this.firstJoinActions = new ActionsLoader().load(config, "first-join", false);
        this.joinActions = new ActionsLoader().load(config, "join", false);

        this.milestones = new RewardLoader().getRewards(config);
        this.firstJoinRewards = new FirstJoinRewardLoader().getRewards(config);

        this.firstJoinRewardsEnabled = config.getBoolean("first-join-rewards.enabled", false);

        this.bungeecord = config.getBoolean("bungeecord", false);
        this.notifyUpdateOnJoin = config.getBoolean("notify-update-on-join", true);
    }

    public String getLang() {
        return lang;
    }

    public Actions getWelcomeActions() {
        return welcomeActions;
    }

    public Actions getFirstJoinActions() {
        return firstJoinActions;
    }

    public Actions getJoinActions() {
        return joinActions;
    }

    public List<Reward> getMilestones() {
        return milestones;
    }

    public List<Reward> getFirstJoinRewards() {
        return firstJoinRewards;
    }

    public boolean isFirstJoinRewards() {
        return firstJoinRewardsEnabled;
    }

    public boolean isBungeecord() {
        return bungeecord;
    }

    public boolean isNotifyUpdateOnJoin() {
        return notifyUpdateOnJoin;
    }
}
