package me.sharkz.ultrawelcome.bukkit.files;

import me.sharkz.ultrawelcome.bukkit.UW;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class PlayersData {

    private final YamlConfiguration config = new YamlConfiguration();
    private final File file = new File(UW.I.getDataFolder(), "players.yml");

    private final String storagetype = UW.I.getConfiguration().getConfig().getString("player-storage-type", "uuid");
    private final String datatype = UW.I.getConfiguration().getConfig().getString("data-storage-type", "spigot");

    public PlayersData() {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        config.options().header("====================================== #\n" +
                "UltraWelcome                           #\n" +
                "Version " + UW.I.getDescription().getVersion() + "             #\n" +
                "Developed by Sharkz, Quantum           #\n" +
                "                                       #\n" +
                "Don't edit this file if you don't      #\n" +
                "know what you are doing                #\n" +
                "====================================== #\n");
        config.options().copyHeader(true);

        try {
            config.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            UW.L.warning("Cannot load players.yml !");
            e.printStackTrace();
        }

        new BukkitRunnable() {
            @Override
            public void run() {
                save();
            }
        }.runTaskTimer(UW.I, 0L, 400L);
    }

    public void save() {
        if(config == null || file == null)
            return;
        try {
            config.save(file);
        } catch (IOException e) {
            UW.L.warning("Cannot save players.yml !");
            e.printStackTrace();
        }
    }

    /**
     * Get the player UUID or NAME based on the storage type set.
     *
     * @param player
     * @return
     */
    public String getPlayer(OfflinePlayer player) {
        if (storagetype.equals("uuid")) {
            return player.getUniqueId().toString();
        }
        return player.getName();
    }

    /**
     * Get the player points from the config.
     *
     * @param player
     * @return
     */
    public int getPoints(OfflinePlayer player) {
        return config.getInt(getPlayer(player) + ".points");
    }

    /**
     * Add player points to the previously saved in the config.
     *
     * @param player
     * @param points
     */
    public void addPoints(OfflinePlayer player, int points) {
        setPoints(player, getPoints(player) + points);
    }

    /**
     * Set player points overriding the previously saved in the config.
     *
     * @param player
     * @param points
     */
    public void setPoints(OfflinePlayer player, int points) {
        config.set(getPlayer(player) + ".points", points);
    }

    /**
     * Removes player points to the previously saved in the config.
     *
     * @param player
     * @param points
     */
    public void removePoints(OfflinePlayer player, int points) {
        setPoints(player, getPoints(player) - points);
    }

    /**
     * Get all saved players with their points from the config
     *
     * @return
     */
    public Map<String, Integer> getPoints() {
        Map<String, Integer> tmp = new HashMap<>();
        config.getKeys(false).forEach(s -> tmp.put(s, config.getInt(s + ".points")));
        return sortByValue(tmp);
    }

    /**
     * 
     * @param map
     * @param <K>
     * @param <V>
     * @return
     */
    private <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list)
            result.put(entry.getKey(), entry.getValue());

        return result;
    }

    /**
     * Gets the player number from the config
     *
     * @param player
     * @return Player number
     */
    public int getPlayerNumber(OfflinePlayer player) {
        return config.getInt(getPlayer(player) + ".number");
    }

    public void addPlayer(OfflinePlayer player) {
        config.set(getPlayer(player) + ".number", config.getKeys(false).stream().filter(s -> config.isSet(s + ".number")).count());
    }

    public boolean hasPlayedBefore(OfflinePlayer player) {
        return datatype.equalsIgnoreCase("yml") ? config.isSet(getPlayer(player)) : player.hasPlayedBefore();
    }

    /**
     * Older versions doesn't support "playerNumber" so we prevent throwing some errors
     * in newer versions by adding the "playerNumber".
     */
    public void updateForOlderVersions() {
        AtomicInteger playerNumber = new AtomicInteger((int) config.getKeys(false).stream().filter(s -> config.isSet(s + ".number")).count());
        if (playerNumber.get() == config.getKeys(false).size())
        config.getKeys(false).forEach(s -> {
            if (!config.isConfigurationSection(s) && !config.isSet(s + ".number")) {
                int points = config.getInt(s);
                config.set(s + ".points", points);
                config.set(s + ".number", playerNumber.get());
                playerNumber.addAndGet(1);
            }
        });
    }
}
