package me.sharkz.ultrawelcome.bukkit;

import com.google.common.io.Files;
import me.sharkz.ultrawelcome.bukkit.commands.UWCmd;
import me.sharkz.ultrawelcome.bukkit.commands.WelcomeCmd;
import me.sharkz.ultrawelcome.bukkit.files.Config;
import me.sharkz.ultrawelcome.bukkit.files.PlayersData;
import me.sharkz.ultrawelcome.bukkit.listeners.ChatEvent;
import me.sharkz.ultrawelcome.bukkit.listeners.UListener;
import me.sharkz.ultrawelcome.bukkit.placeholders.PlaceholderImpl;
import me.sharkz.ultrawelcome.bukkit.rewards.actions.Broadcast;
import me.sharkz.ultrawelcome.bukkit.utils.BukkitChannel;
import me.sharkz.ultrawelcome.bukkit.utils.Lang;
import me.sharkz.ultrawelcome.bukkit.utils.UpdateChecker;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import net.milkbowl.vault.economy.Economy;
import org.bstats.bukkit.Metrics;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

public class UW extends JavaPlugin {

    public static UW I;
    public static Logger L;
    public static boolean vaultEnabled, PAPIEnabled, newVersion;
    public static final int configVersion = 8;

    public Economy economy;
    public Map<UUID, Broadcast> broadcasts = new HashMap<>();

    private Config config;
    private PlayersData players;

    public UW() {
        I = this;
    }

    @Override
    public void onEnable() {
        L = getLogger();

        // Header start
        L.info("========== ULTRA WELCOME ==========");
        L.info("Developed by : Sharkz, Quantum");
        L.info("Version : " + getDescription().getVersion());
        L.info("Do not hesitate to positively rate the plugin on spigotmc.org");
        L.info("===================================");

        // Dependencies
        vaultEnabled = PAPIEnabled = newVersion = false;
        checkDependencies();

        // Updates
        checkVersion();

        // Metrics
        new Metrics(this, 8665);

        if (!getDataFolder().exists()) getDataFolder().mkdir();
        config = new Config();

        if (getConfiguration().isBungeecord()) {
            // Server compatibility
            checkIfBungee();

            // Messaging Channel
            getServer().getMessenger().registerIncomingPluginChannel(this, CommonUtils.CHANNEL, new BukkitChannel());
            getServer().getMessenger().registerOutgoingPluginChannel(this, CommonUtils.CHANNEL);
        } else {
            // Configuration
            if (getConfiguration().getConfig().getInt("config_version") != configVersion) {
                L.warning("The configuration file is outdated !");
                try {
                    Files.copy(new File(getDataFolder(), "config.yml"), new File(getDataFolder(), "config.old.yml"));
                    L.info("Your configuration has been saved & replaced !");
                } catch (IOException e) {
                    L.warning("Cannot backup your configuration ...");
                    e.printStackTrace();
                    getPluginLoader().disablePlugin(this);
                }
                new File(getDataFolder(), "config.yml").delete();
                config = new Config();
            }

            players = new PlayersData();
            players.updateForOlderVersions();

            // Lang file
            loadLang();

            // Commands
            String welcomePermission = getConfiguration().getConfig().getString("welcomeCommand.permission", "");
            if (!welcomePermission.isEmpty()) {
                getServer().getPluginManager().addPermission(new Permission(welcomePermission, "Allow you to welcome new players"));
            }

            new UWCmd();
            if (!getConfiguration().getConfig().getString("welcomeCommand.command", "welcome").isEmpty()) {
                new WelcomeCmd(getConfiguration().getConfig().getString("welcomeCommand.command", "welcome"), Util.getPermission(getConfig(), "welcomeCommand.permission"), getConfiguration().getConfig().getStringList("welcomeCommand.aliases"), getConfiguration().getConfig().getInt("welcomeCommand.time-limit", 12));
            }
        }

        // Listeners
        getServer().getPluginManager().registerEvents(new UListener(), this);
        getServer().getPluginManager().registerEvents(new ChatEvent(), this);
    }

    /**
     * Check if a new version of the plugin exist
     */
    private void checkVersion() {
        new UpdateChecker(this, 82991)
                .getVersion(s -> {
                    if (getDescription().getVersion().equalsIgnoreCase(s))
                        L.info("You're running the last version of the plugin !");
                    else {
                        L.warning("A new version of UltraWelcome is available on spigotmc.org !");
                        newVersion = true;
                    }
                });
    }

    /**
     * Check if dependencies are enabled or not
     */
    private void checkDependencies() {
        if (getServer().getPluginManager().getPlugin("Vault") != null) {
            RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
            if (rsp == null)
                L.warning("Cannot hook into Vault.");
            else {
                economy = rsp.getProvider();
                L.info("Successfully hooked into Vault");
                vaultEnabled = true;
            }
        }

        if (getServer().getPluginManager().getPlugin("PlaceholderAPI") != null) {
            L.info("Successfully hooked into PlaceholderAPI");
            new PlaceholderImpl().register();
            PAPIEnabled = true;
        }
    }

    @Override
    public void onDisable() {
        L.info("Thanks for using UltraWelcome !");
        if (!getConfiguration().isBungeecord()) players.save();
    }

    private void checkIfBungee() {
        // we check if the server is Spigot/Paper
        if (!getServer().getVersion().contains("Spigot") && !getServer().getVersion().contains("Paper")) {
            getLogger().severe("You probably run CraftBukkit... Please update atleast to spigot for this to work...");
            getLogger().severe("Plugin disabled!");
            getServer().getPluginManager().disablePlugin(this);
        }
    }

    public void loadLang() {
        String lang = getConfiguration().getLang();
        File langFile = new File(getDataFolder() + "/lang", lang + ".lang");
        YamlConfiguration config = new YamlConfiguration();

        config.options().header("All messages support %prefix% placeholder\n" +
                "Messages starting with \"points\" (except \"points_command_help\") support %player% and %points%\n" +
                "For yaml multi line messages see uw.roch-blondiaux.com/YamlFormatting");
        config.options().copyHeader(true);

        // Create lang file with the name specified by the user if it doesn't exist
        if (!langFile.exists()) {
            try {
                langFile.getParentFile().mkdirs();
                langFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            config.load(langFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }

        // For avoiding null values we put default ones in the lang file if they doesn't exist
        for (Lang item : Lang.values()) {
            if (config.getString(item.getPath()) == null) {
                config.set(item.getPath(), item.getDefault());
            }
        }

        try {
            config.save(langFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // We set the configuration variable in the Lang enum so it can use it when we request a message
        Lang.setFile(config);
    }

    public Config getConfiguration() {
        return config;
    }

    public PlayersData getPlayers() {
        return players;
    }
}
