package me.sharkz.ultrawelcome.bukkit.loader;

import com.cryptomorin.xseries.XItemStack;
import me.sharkz.ultrawelcome.bukkit.rewards.Actions;
import me.sharkz.ultrawelcome.bukkit.rewards.actions.*;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import me.sharkz.ultrawelcome.common.utils.CommonUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.permissions.Permission;

import java.util.ArrayList;
import java.util.List;

public class ActionsLoader implements Loader<Actions> {

    @Override
    public Actions load(FileConfiguration config, String path) {
        return null;
    }

    public Actions load(FileConfiguration config, String path, boolean welcomeActions) {
        // Load broadcasts
        List<Broadcast> broadcasts = new ArrayList<>();
        if (config.isConfigurationSection(path + ".broadcast")) {
            if (config.getBoolean(path + ".broadcast.enabled", false)) {
                // Get action type (WelcomeActions have sender option)
                ActionType broadcastType;
                if (welcomeActions) {
                    broadcastType = ActionType.valueOf(config.getString(path + ".broadcast.type", "global").toUpperCase()) == ActionType.GLOBAL ?
                            (config.getString(path + ".broadcast.sender", "broadcast").equalsIgnoreCase("broadcast") ?
                                    ActionType.GLOBAL_BC : ActionType.GLOBAL_CHAT) :
                            (config.getString(path + ".broadcast.sender", "broadcast").equalsIgnoreCase("broadcast") ?
                                    ActionType.SINGLE_BC : ActionType.SINGLE_CHAT);
                } else {
                    broadcastType = ActionType.valueOf(config.getString(path + ".broadcast.type", "global").toUpperCase());
                }

                // Get single or multiple broadcasts
                if (config.isSet(path + ".broadcast.broadcast")) {
                    String broadcast = CommonUtils.color(Util.getSingleOrRandom(config, path + ".broadcast.broadcast"));
                    broadcasts.add(new Broadcast(broadcastType, broadcast, null));
                } else if (config.isConfigurationSection(path + ".broadcast.broadcasts")) {
                    config.getConfigurationSection(path + ".broadcast.broadcasts").getKeys(false).forEach(i -> {
                        String _path = path + ".broadcast.broadcasts." + i;
                        String broadcast = CommonUtils.color(Util.getSingleOrRandom(config, _path + ".message"));
                        Permission permission = Util.getPermission(config, _path + ".permission");
                        broadcasts.add(new Broadcast(broadcastType, broadcast, permission));
                    });
                }
            }
        }

        // Load firework
        List<Firework> fireworks = new ArrayList<>();
        if (config.isConfigurationSection(path + ".firework")) {
            if (config.getBoolean(path + ".firework.enabled", false)) {
                // Get action type
                ActionType fireworkType = ActionType.valueOf(config.getString(path + ".firework.type", "global").toUpperCase());

                // Get single or multiple fireworks
                if (config.isSet(path + ".firework.firework")) {
                    YamlConfiguration fw = new YamlConfiguration();
                    fw.set("material", "FIREWORK_ROCKET");
                    fw.set("power", config.getInt(path + ".firework.firework.power", 1));
                    fw.set("firework.1", config.getConfigurationSection(path + ".firework.firework"));
                    fireworks.add(new Firework(fireworkType, (FireworkMeta) XItemStack.deserialize(fw).getItemMeta(), null));
                } else if (config.isConfigurationSection(path + ".firework.fireworks")) {
                    config.getConfigurationSection(path + ".firework.fireworks").getKeys(false).forEach(i -> {
                        String _path = path + ".firework.fireworks." + i;
                        YamlConfiguration fw = new YamlConfiguration();
                        fw.set("material", "FIREWORK_ROCKET");
                        fw.set("power", config.getInt(_path + "firework.power", 1));
                        fw.set("firework.1", config.getConfigurationSection(_path + ".firework"));
                        FireworkMeta fireworkMeta = (FireworkMeta) XItemStack.deserialize(fw).getItemMeta();
                        Permission permission = Util.getPermission(config, _path + ".permission");
                        fireworks.add(new Firework(fireworkType, fireworkMeta, permission));
                    });
                }
            }
        }

        // Load commands
        List<Command> commands = new ArrayList<>();
        if (config.isConfigurationSection(path + ".command")) {
            if (config.getBoolean(path + ".command.enabled", false)) {
                // Get action type
                ActionType commandType = ActionType.valueOf(config.getString(path + ".command.type", "global").toUpperCase());

                // Get single or multiple commands
                if (config.isSet(path + ".command.command")) {
                    String command = Util.getSingleOrRandom(config, path + ".command.command");
                    commands.add(new Command(commandType, command, null));
                } else if (config.isConfigurationSection(path + ".command.commands")) {
                    config.getConfigurationSection(path + ".command.commands").getKeys(false).forEach(i -> {
                        String _path = path + ".command.commands." + i;
                        String command = Util.getSingleOrRandom(config, _path + ".command");
                        Permission permission = Util.getPermission(config, _path + ".permission");
                        commands.add(new Command(commandType, command, permission));
                    });
                }
            }
        }

        // Load titles
        List<Title> titles = new ArrayList<>();
        if (config.isConfigurationSection(path + ".title")) {
            if (config.getBoolean(path + ".title.enabled", false)) {
                // Get action type
                ActionType titleType = ActionType.valueOf(config.getString(path + ".title.type", "global").toUpperCase());

                // Get single or multiple titles
                if (config.isConfigurationSection(path + ".title.title")) {
                    String _path = path + ".title.title";
                    String title = Util.getSingleOrRandom(config, _path + ".title");
                    String subtitle = Util.getSingleOrRandom(config, _path + ".subtitle");
                    int fade_in = config.getInt(_path + ".fade-in", 10);
                    int stay = config.getInt(_path + ".stay", 20);
                    int fade_out = config.getInt(_path + ".fade-out", 10);
                    titles.add(new Title(titleType, title, subtitle, fade_in, stay, fade_out, null));
                } else if (config.isConfigurationSection(path + ".title.titles")) {
                    config.getConfigurationSection(path + ".title.titles").getKeys(false).forEach(i -> {
                        String _path = path + ".title.titles." + i;
                        String title = Util.getSingleOrRandom(config, _path + ".title");
                        String subtitle = Util.getSingleOrRandom(config, _path + ".subtitle");
                        int fade_in = config.getInt(_path + ".fade-in", 10);
                        int stay = config.getInt(_path + ".stay", 20);
                        int fade_out = config.getInt(_path + ".fade-out", 10);
                        Permission permission = Util.getPermission(config, _path + ".permission");
                        titles.add(new Title(titleType, title, subtitle, fade_in, stay, fade_out, permission));
                    });
                }
            }
        }

        // Load money
        List<Money> monies = new ArrayList<>();
        if (welcomeActions && config.isConfigurationSection(path + ".reward")) {
            if (config.getBoolean(path + ".reward.enabled", false)) {
                // Get single or multiple money
                if (config.isConfigurationSection(path + ".reward.reward")) {
                    String _path = path + ".reward.reward";
                    int money = config.getInt(_path + ".money", 0);
                    String message = CommonUtils.color(Util.getSingleOrRandom(config, _path + ".message"));
                    monies.add(new Money(money, message, null));
                } else if (config.isConfigurationSection(path + ".reward.rewards")) {
                    config.getConfigurationSection(path + ".reward.rewards").getKeys(false).forEach(i -> {
                        String _path = path + ".reward.rewards." + i;
                        int money = config.getInt(_path + ".money", 0);
                        String message = CommonUtils.color(Util.getSingleOrRandom(config, _path + ".message"));
                        Permission permission = Util.getPermission(config, _path + ".permission");
                        monies.add(new Money(money, message, permission));
                    });
                }
            }
        }

        return new Actions(broadcasts, fireworks, commands, titles, monies);
    }

    @Override
    public void save(Actions object, FileConfiguration config, String path) {

    }
}
