package me.sharkz.ultrawelcome.bukkit.loader;

import com.cryptomorin.xseries.XItemStack;
import me.sharkz.ultrawelcome.bukkit.rewards.Reward;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.stream.Collectors;

public class FirstJoinRewardLoader implements Loader<Reward> {

    @Override
    public Reward load(FileConfiguration config, String path) {
        String msg = Util.getSingleOrRandom(config, path + ".message");
        double money = config.getDouble(path + ".money");
        String cmd = Util.getSingleOrRandom(config, path + ".command");
        if (config.get(path + ".items") != null && config.isConfigurationSection(path + ".items")) {
            ConfigurationSection sec = config.getConfigurationSection(path + ".items");
            List<ItemStack> items = new ArrayList<>();
            for (String key : sec.getKeys(false)) {
                if (!config.isConfigurationSection(path + ".items." + key)) continue;
                ItemStack i = XItemStack.deserialize(config.getConfigurationSection(path + ".items." + key));
                if (i == null) continue;
                items.add(i);
            }
            return new Reward(msg, 0, money, cmd, items);
        }
        return new Reward(msg, 0, money, cmd);
    }

    @Override
    public void save(Reward object, FileConfiguration config, String path) {
    }

    /**
     * @param config
     * @return
     */
    public List<Reward> getRewards(FileConfiguration config) {
        if (config.isConfigurationSection("first-join-rewards")) {
            String firstJoinRewardType = config.getString("first-join-rewards.type", "single");
            ConfigurationSection sec = config.getConfigurationSection("first-join-rewards");
            List<String> rewardsKeys = new ArrayList<>(sec.getKeys(false));
            rewardsKeys.remove("enabled");
            rewardsKeys.remove("type");
            if (rewardsKeys.isEmpty()) return new ArrayList<>();
            List<Reward> rewards = rewardsKeys.stream().map(s -> load(config, sec.getCurrentPath() + "." + s))
                    .filter(reward -> reward.hasCommand() || reward.hasItems() || reward.hasMessage() || reward.hasMoney())
                    .collect(Collectors.toList());
            switch (firstJoinRewardType) {
                case "single": {
                    return Collections.singletonList(rewards.get(0));
                }
                case "all": {
                    return rewards;
                }
                case "random": {
                    return Collections.singletonList(rewards.get(new Random().nextInt(rewardsKeys.size())));
                }
            }
        }
        return new ArrayList<>();
    }
}
