package me.sharkz.ultrawelcome.bukkit.loader;

import org.bukkit.configuration.file.FileConfiguration;

public interface Loader<T> {

    /**
     * Load object from configuration
     *
     * @param config configuration file
     * @param path   path to object
     * @return Java object
     */
    T load(FileConfiguration config, String path);

    /**
     * Save java object to configuration
     *
     * @param object java object to save
     * @param config configuration file
     * @param path   path to object
     */
    void save(T object, FileConfiguration config, String path);
}
