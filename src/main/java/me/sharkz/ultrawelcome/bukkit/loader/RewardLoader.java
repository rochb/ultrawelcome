package me.sharkz.ultrawelcome.bukkit.loader;

import com.cryptomorin.xseries.XItemStack;
import me.sharkz.ultrawelcome.bukkit.rewards.Reward;
import me.sharkz.ultrawelcome.bukkit.utils.Util;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RewardLoader implements Loader<Reward> {

    @Override
    public Reward load(FileConfiguration config, String path) {
        String msg = Util.getSingleOrRandom(config, path + ".message");
        int pts = config.getInt(path + ".points");
        double money = config.getDouble(path + ".money");
        String cmd = Util.getSingleOrRandom(config, path + ".command");
        if (config.get(path + ".items") != null && config.isConfigurationSection(path + ".items")) {
            ConfigurationSection sec = config.getConfigurationSection(path + ".items");
            List<ItemStack> items = new ArrayList<>();
            for (String key : sec.getKeys(false)) {
                if (!config.isConfigurationSection(path + ".items." + key)) continue;
                ItemStack i = XItemStack.deserialize(config.getConfigurationSection(path + ".items." + key));
                if (i == null) continue;
                items.add(i);
            }
            return new Reward(msg, pts, money, cmd, items);
        }
        return new Reward(msg, pts, money, cmd);
    }

    @Override
    public void save(Reward object, FileConfiguration config, String path) {
    }

    /**
     * @param config
     * @return
     */
    public List<Reward> getRewards(FileConfiguration config) {
        if (config.isConfigurationSection("rewards")) {
            ConfigurationSection sec = config.getConfigurationSection("rewards");
            return sec.getKeys(false).stream().map(s -> load(config, sec.getCurrentPath() + "." + s))
                    .filter(reward -> reward.hasPoints() && (reward.hasCommand() || reward.hasMoney() || reward.hasItems()))
                    .collect(Collectors.toList());
        } else
            return new ArrayList<>();
    }
}
