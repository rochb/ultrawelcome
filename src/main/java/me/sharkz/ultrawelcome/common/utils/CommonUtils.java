package me.sharkz.ultrawelcome.common.utils;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

import java.io.*;
import java.util.Base64;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CommonUtils {

    public static final String CHANNEL = "sharkz:uwelcome";

    public static TextComponent newVersionMessage() {
        TextComponent message = new TextComponent("A new version of ");
        message.setColor(ChatColor.GREEN);
        TextComponent pl1 = new TextComponent("ULTRA ");
        TextComponent pl2 = new TextComponent("WELCOME ");
        pl1.setColor(ChatColor.RED);
        pl1.setBold(true);
        pl2.setColor(ChatColor.AQUA);
        pl2.setBold(true);
        message.addExtra(pl1);
        message.addExtra(pl2);
        TextComponent msg = new TextComponent("is available. ");
        msg.setColor(ChatColor.GREEN);
        message.addExtra(msg);
        TextComponent link = new TextComponent("Click here to download !");
        link.setColor(ChatColor.GOLD);
        link.setBold(true);
        link.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/resources/82991/"));
        message.addExtra(link);
        return message;
    }

    public static String serialize(Serializable o) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        oos.close();
        return Base64.getEncoder().encodeToString(baos.toByteArray());
    }

    public static Object deserialize(String s) throws IOException, ClassNotFoundException {
        byte[] data = Base64.getDecoder().decode(s);
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
        Object o = ois.readObject();
        ois.close();
        return o;
    }

    /**
     * Color message
     *
     * @param message to color
     * @return colored message
     */
    public static String color(String message) {
        Pattern hexPattern = Pattern.compile("(&#([\\da-f]{3}){1,2})");
        Matcher matcher = hexPattern.matcher(message);
        while (matcher.find()) {
            final ChatColor hexColor = ChatColor.of(matcher.group().substring(1));
            final String before = message.substring(0, matcher.start());
            final String after = message.substring(matcher.end());
            message = before + hexColor + after;
            matcher = hexPattern.matcher(message);
        }

        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static List<String> color(List<String> messages) {
        return messages.stream().map(CommonUtils::color).collect(Collectors.toList());
    }

    /**
     * Reverse color
     *
     * @param message to reverse
     * @return reversed message
     */
    public static String reverseColor(String message) {
        return ChatColor.stripColor(message);
    }

    /**
     * Reverse color
     *
     * @param messages to reverse
     * @return reversed messages
     */
    public static List<String> reverseColor(List<String> messages) {
        return messages.stream().map(CommonUtils::reverseColor).collect(Collectors.toList());
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static List<String> filterTabCompleteOptions(Collection<String> options, String... args) {
        String lastArg = "";
        if (args.length > 0) {
            lastArg = args[(args.length - 1)].toLowerCase();
        }
        List<String> Options = new LinkedList<>(options);
        for (int i = 0; i < Options.size(); i++) {
            if (!Options.get(i).toLowerCase().startsWith(lastArg)) {
                Options.remove(i--);
            }
        }
        return Options;
    }

}
